#!/bin/sh
#
# Wrapper script to run tox. Arguments are passed directly to tox.

# Exit on failure
set -e

# Change to this script's directory
cd "$( dirname "$0")"

# Execute tox runner, logging command used
set -x
docker build -t gsuite-synctool .
docker run \
    -it --rm \
    -v $(pwd)/htmlcov:/usr/src/app/build/py3/htmlcov:rw \
    -e TOXINI_SITEPACKAGES=True \
    --entrypoint /bin/sh gsuite-synctool -c "tox $*"
