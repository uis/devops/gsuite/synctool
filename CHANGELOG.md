# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.5] - 2024-09-11

### Changed

- No longer use lastLoginTime from Google user API, especially for determining users to delete
- Calculate and log numbers of users to delete even if not going to perform those deletions

## [1.0.4] - 2024-07-08

### Fixed

- Catch/Ignore users with no lastLoginTime field, and ensure they aren't accidentally deleted

## [1.0.3] - 2023-02-27

### Changed

- Reduce period after which suspended users are marked for scanning from six to one month

## [1.0.2] - 2023-01-01

### Changed

- Delete all accounts cancelled for 2 years ago

## [1.0.1] - 2023-08-29

### Fixed

- Error handling when there are insufficient licences to assign

## [1.0.0] - 2023-06-22

### Changed

- Change `--really-do-this` to `--write`.

## [0.14.2] - 2023-05-23

### Changed

- MYDRIVE_SHARED_ACTION_SCAN's value is now `scan-folders` instead of `scan`

## [0.14.1] - 2023-04-27

### Changed

- Cap license list page size to 200 following Google API bug when using default 500

## [0.14.0] - 2023-03-22

### Added

- Delete scanned users with no shared files.

## [0.13.0] - 2023-03-06

### Added

- Custom Schema reading and writing to mark users' MyDrives to be scanned

## [0.12.0] - 2022-12-08 - unreleased

## [0.11.1] - 2022-11-25

### Changed

- Suspended members are removed from groups

## [0.11.0] - 2022-10-10

### Changed

- Lookup users and groups retrieved via API Gateway instead of LDAP

## [0.10.0] - 2022-03-02

### Added

- Support for licence management

## [0.9.3] - 2020-11-10

### Changed

- Updates to group settings conditional on --group-settings argument

## [0.9.2] - 2020-11-10

### Added

- Group and Institution syncing

### Changed

- Improved error handling

## [0.9.1] - 2019-09-25

### Changed

- Improved display name usage

### Fixed

- Retry on error responses
- Set organisational unit when adding users

## [0.9.0] - 2019-04-30

### Added

- Initial implementation
