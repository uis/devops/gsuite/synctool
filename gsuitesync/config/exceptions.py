"""
Configuration Exceptions

"""


class ConfigurationError(RuntimeError):
    """
    Base class for all configuration errors.

    """


class ConfigurationNotFound(ConfigurationError):
    """
    A suitable configuration could not be located.

    """

    def __init__(self):
        return super().__init__("Could not find any configuration file")
