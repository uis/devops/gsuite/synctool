"""
Synchronisation limits.

"""
import dataclasses
import numbers
import typing

from .mixin import ConfigurationDataclassMixin


@dataclasses.dataclass
class Configuration(ConfigurationDataclassMixin):
    """
    Configuration for synchronisation limits.

    """

    # The abort_... settings below are safety limits and will abort the run if the limits are
    # violated. They are there to define the "sane limits" for an update.

    # Refuse to perform sync if we are to "touch" more than this percentage of users. The
    # percentage of users "touched" is calculated as
    #
    #   (new google users + modified google users) / max(1, total google users)
    #
    # where "modified" includes metadata changes and suspension/restoration. As such this
    # calculated percentage can be greater than 100. Set to null to have no limit. Default: null.
    abort_user_change_percentage: typing.Union[None, numbers.Real] = None

    # Refuse to perform sync if we are to "touch" more than this percentage of licence
    # assignments. The percentage of licence assignments "touched" is calculated as
    #
    #   (added licences + removed licences) / max(1, total licences)
    #
    # As such this calculated percentage can be greater than 100. Set to null to have no limit.
    # Default: null.
    abort_licence_change_percentage: typing.Union[None, numbers.Real] = None

    # Refuse to perform sync if we are to "touch" more than this percentage of groups. The
    # percentage of groups "touched" is calculated as
    #
    #   (new google groups + modified google groups) / max(1, total google groups)
    #
    # where "modified" includes metadata changes and deletion. As such this calculated
    # percentage can be greater than 100. Set to null to have no limit. Default: null.
    abort_group_change_percentage: typing.Union[None, numbers.Real] = None

    # Refuse to perform sync if we are to "touch" more than this percentage of overall group
    # memberships. The percentage of group memberships "touched" is calculated as
    #
    #   (new memberships + deleted memberships) / max(1, total google group memberships)
    #
    # As such this calculated percentage can be greater than 100. Set to null to have no
    # limit. Default: null.
    abort_member_change_percentage: typing.Union[None, numbers.Real] = None

    # The max_... settings below will not abort the run if the number of items affected is greater
    # than the specified number. Instead the number of items affected is capped to that number. The
    # selection of which items are included in the capped number is arbitrary.

    # Limit the number of new user creations per run. This is an absolute number. Set to None to
    # have no limit.
    max_new_users: typing.Union[None, numbers.Real] = None

    # Limit the number of new group creations per run. This is an absolute number. Set to None to
    # have no limit.
    max_new_groups: typing.Union[None, numbers.Real] = None

    # Limit the number of user suspensions per run. This is an absolute number. Set to None to
    # have no limit.
    max_suspended_users: typing.Union[None, numbers.Real] = None

    # Limit the number of user deletions per run. This is an absolute number. Set to None to have
    # no limit. Defaults to zero to avoid deletions if using outdated configuration.
    max_deleted_users: typing.Union[None, numbers.Real] = 0

    # Limit the number of group deletions per run. This is an absolute number. Set to None to have
    # no limit.
    max_deleted_groups: typing.Union[None, numbers.Real] = None

    # Limit the number of user un-suspensions (reactivations) per run. This is an absolute number.
    # Set to None to have no limit.
    max_reactivated_users: typing.Union[None, numbers.Real] = None

    # Limit the number of user un-deletions (restorations) per run. This is an absolute number.
    # Set to None to have no limit.
    max_restored_users: typing.Union[None, numbers.Real] = None

    # Limit the number of user's to be marked to have their MyDrive scanned. This is
    # an absolute number. Set to null to have no limit. Default: null
    max_mydrive_scan_users: typing.Union[None, numbers.Real] = None

    # Limit the number of user metadata changes per run. This is an absolute number. Set to None to
    # have no limit.
    max_updated_users: typing.Union[None, numbers.Real] = None

    # Limit the number of licence assignments to create per run. This is an absolute number. Set
    # to None to have no limit.
    max_add_licence: typing.Union[None, numbers.Real] = None

    # Limit the number of licence assignments to delete per run. This is an absolute number. Set
    # to None to have no limit.
    max_remove_licence: typing.Union[None, numbers.Real] = None

    # Limit the number of group metadata changes per run. This is an absolute number. Set to None
    # to have no limit.
    max_updated_groups: typing.Union[None, numbers.Real] = None

    # Limit the total number of group members to insert per run. This is an absolute number. Set
    # to None to have no limit.
    max_inserted_members: typing.Union[None, numbers.Real] = None

    # Limit the total number of group members to delete per run. This is an absolute number. Set
    # to None to have no limit.
    max_deleted_members: typing.Union[None, numbers.Real] = None
