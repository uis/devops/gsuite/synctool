"""
Synchronisation configuration.

"""
import dataclasses
import numbers
import typing

from .mixin import ConfigurationDataclassMixin


@dataclasses.dataclass
class Configuration(ConfigurationDataclassMixin):
    # A regular expression which is used to match the organization unit path for Google users who
    # should be excluded from the list returned by Google. Those users do not exist for the
    # purposes of the rest of the sync and so if they appear in the list of managed users this
    # script will attempt to re-add them and fail in the process. Use this setting for users who
    # are managed completely outside of this script.
    ignore_google_org_unit_path_regex: typing.Union[str, None] = None

    # The organization unit path in which new accounts are placed
    new_user_org_unit_path: str = "/"

    # Suffix appended to the names of groups created in Google. The Google group name will be
    # "{groupName}{group_name_suffix}", where {groupName} is the Lookup group name.
    group_name_suffix: str = " from lookup.cam.ac.uk"

    # Settings to be applied to groups in Google. These settings are applied to both new and
    # existing groups imported from Lookup.
    # See https://developers.google.com/admin-sdk/groups-settings/v1/reference/groups#json
    group_settings: dict = dataclasses.field(
        default_factory=lambda: {
            "whoCanJoin": "INVITED_CAN_JOIN",
            "whoCanViewMembership": "ALL_IN_DOMAIN_CAN_VIEW",
            "whoCanViewGroup": "ALL_MEMBERS_CAN_VIEW",
            "whoCanPostMessage": "ALL_IN_DOMAIN_CAN_POST",
            "allowWebPosting": "false",
            "messageModerationLevel": "MODERATE_ALL_MESSAGES",
            "includeInGlobalAddressList": "true",
            "whoCanLeaveGroup": "NONE_CAN_LEAVE",
            "whoCanContactOwner": "ALL_MANAGERS_CAN_CONTACT",
            "whoCanModerateMembers": "OWNERS_ONLY",
            "whoCanDiscoverGroup": "ALL_IN_DOMAIN_CAN_DISCOVER",
        }
    )

    # Inter-batch delay in seconds. This is useful to avoid hitting Google rate limits.
    inter_batch_delay: numbers.Real = 5

    # Batch size for Google API calls. Google supports batching requests together into one API
    # call.
    batch_size: int = 50

    # Number of times to retry HTTP requests if a HTTP failure response is received
    http_retries: int = 5

    # Delay in seconds between retying a request that has failed
    http_retry_delay: numbers.Real = 5
