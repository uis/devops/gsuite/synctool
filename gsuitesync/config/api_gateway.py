"""
API Gateway authentication.

"""
import dataclasses
import logging

from .mixin import ConfigurationDataclassMixin


LOG = logging.getLogger(__name__)


@dataclasses.dataclass
class Configuration(ConfigurationDataclassMixin):
    """
    Configuration of API Gateway access credentials.

    """

    # Path to on-disk JSON credentials used when accessing APIs through API Gateway.
    credentials: str
