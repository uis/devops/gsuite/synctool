"""
Google Subscription licensing.

"""
import dataclasses
import typing

from .mixin import ConfigurationDataclassMixin


@dataclasses.dataclass
class Configuration(ConfigurationDataclassMixin):
    """
    Configuration for applying subscription licensing to users in Google.

    IDs and SKUs available from:
    https://developers.google.com/admin-sdk/licensing/v1/how-tos/products

    """

    # Customer ID for your Google Workspace domain. Licensing API cannot use `domain` like others.
    customer_id: str

    # Product ID of the Google Workspace product (e.g. 101031 for Google Workspace for Education)
    # Use None if no subscriptions are allocated to the Google Workspace billing
    product_id: typing.Optional[str] = None

    # Dict of SKU IDs with their maximum size to assign licensed users to
    # e.g.
    #   "1010310008": 30000 for Google Workspace for Education Plus
    #   "1010310009": 2000  for Google Workspace for Education Plus (Staff)
    licensed_skus: typing.Dict[str, int] = dataclasses.field(default_factory=dict)
