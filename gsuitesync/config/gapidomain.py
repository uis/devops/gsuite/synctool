"""
Google Domain management.

"""
import dataclasses
import typing

from .mixin import ConfigurationDataclassMixin


@dataclasses.dataclass
class Configuration(ConfigurationDataclassMixin):
    """
    Configuration for accessing the Google Domain.

    """

    # Name of the domain. (E.g. "example.com".)
    name: str

    # If using a service account with Domain-Wide Delegation, set to the username
    # within the GSuite for the user which has administration rights.
    # Should be an e-mail style name. E.g. "super-admin@example.com". The service
    # account credentials specified in the google_api.auth section are used to
    # perform admin actions as this user.
    # If not using Domain-Wide Delegation (i.e. using an Admin Role with the
    # service account as a member), use null.
    admin_user: str = None

    # Secondary domain or domain alias for groups. If None, the value of "name" is used.
    # Default: None
    groups_domain: typing.Union[str, None] = None

    # Secondary domain or domain alias for institutions. If None, the value of "name" is used.
    # Default: None
    insts_domain: typing.Union[str, None] = None
