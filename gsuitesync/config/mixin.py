"""
Mixin class for dataclass which adds a "from_dict" member which will construct an instance from
a dictionary. Fields which have no default value become required fields.

"""
import dataclasses


class ConfigurationDataclassMixin:
    @classmethod
    def from_dict(cls, dict_):
        """
        Construct an instance from a dict.

        """
        field_names = {field.name for field in dataclasses.fields(cls)}
        required_field_names = {
            field.name for field in dataclasses.fields(cls) if field.default is dataclasses.MISSING
        }

        for key in dict_.keys():
            if key not in field_names:
                raise ValueError(f"Unknown configuration key: {key}")

        for key in required_field_names:
            if key not in dict_:
                raise ValueError(f"{key}: required field not set")

        return cls(**dict_)
