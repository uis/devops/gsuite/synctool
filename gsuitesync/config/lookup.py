"""
Retrieving user information from Lookup API.

"""
import dataclasses
from typing import Optional

from .mixin import ConfigurationDataclassMixin


@dataclasses.dataclass
class Configuration(ConfigurationDataclassMixin):
    """
    Configuration for filtering eligibility and managed users, groups and
    institutions in Lookup API.

    """

    eligible_user_filter: str

    eligible_group_filter: str

    eligible_inst_filter: str

    managed_user_filter: Optional[str] = None

    managed_group_filter: Optional[str] = None

    managed_inst_filter: Optional[str] = None
