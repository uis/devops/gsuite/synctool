import logging
import os
import yaml

from .exceptions import ConfigurationNotFound

# Configuration declarations
from . import api_gateway, gapiauth, gapidomain, limits, lookup, sync, licensing

LOG = logging.getLogger(__name__)


def load_configuration(location=None):
    """
    Load configuration and return a :py:class:`Configuration` instance. Pass a non-None location to
    override the default search path.

    :raises: ConfigurationError if the configuration could not be loaded.

    """
    if location is not None:
        paths = [location]
    else:
        if "GSUITESYNC_CONFIGURATION" in os.environ:
            paths = [os.environ["GSUITESYNC_CONFIGURATION"]]
        else:
            paths = []
        paths.extend(
            [
                os.path.join(os.getcwd(), "gsuitesync.yaml"),
                os.path.join(os.getcwd(), "configuration.yaml"),
                os.path.expanduser("~/.gsuitesync/configuration.yaml"),
                "/etc/gsuitesync/configuration.yaml",
            ]
        )

    valid_paths = [path for path in paths if os.path.isfile(path)]

    if len(valid_paths) == 0:
        LOG.error("Could not find configuration file. Tried:")
        for path in paths:
            LOG.error('"%s"', path)
        raise ConfigurationNotFound()

    with open(valid_paths[0]) as f:
        return yaml.safe_load(f)


def parse_configuration(configuration):
    """
    Parses the multiple parts of configuration using appropriate Configuration classes.
    Returns a dict containing parsed parts of configuration.

    """
    return {
        "api_gateway_auth": api_gateway.Configuration.from_dict(
            configuration.get("api_gateway", {}).get("auth", {})
        ),
        "sync": sync.Configuration.from_dict(configuration.get("sync", {})),
        "gapi_domain": gapidomain.Configuration.from_dict(configuration.get("google_domain", {})),
        "limits": limits.Configuration.from_dict(configuration.get("limits", {})),
        "lookup": lookup.Configuration.from_dict(configuration.get("lookup", {})),
        "gapi_auth": gapiauth.Configuration.from_dict(
            configuration.get("google_api", {}).get("auth", {})
        ),
        "licensing": licensing.Configuration.from_dict(configuration.get("licensing", {})),
    }
