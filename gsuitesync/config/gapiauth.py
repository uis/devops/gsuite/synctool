"""
Google API authentication.

"""
import dataclasses
import logging
import typing

from .mixin import ConfigurationDataclassMixin


LOG = logging.getLogger(__name__)


@dataclasses.dataclass
class Configuration(ConfigurationDataclassMixin):
    """
    Configuration of Google API access credentials.

    """

    # Path to on-disk JSON credentials used when accessing the API.
    credentials: str

    # Path to on-disk JSON credentials used when accessing the API in "read-only" mode. Use this if
    # you want to have a separate "safe" service account which can only read data. If null, use the
    # same credentials for reading and writing.
    read_only_credentials: typing.Union[str, None] = None
