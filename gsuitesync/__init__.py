"""
Synchronise users to GSuite

Usage:
    gsuitesync (-h | --help)
    gsuitesync [--configuration=FILE] [--quiet] [--group-settings] [--just-users]
               [--licensing] [--delete-users] [--write] [--timeout=SECONDS]
               [--cache-dir=DIR]

Options:
    -h, --help                  Show a brief usage summary.

    -q, --quiet                 Reduce logging verbosity.

    -c, --configuration=FILE    Specify configuration file to load.

    -w, --write                 Actually try to make the changes.

    --group-settings            Also update group settings on all groups.
    --just-users                Just compare users not groups and institutions.
    --licensing                 Also update user licence allocations.
    --delete-users              Delete suspended users.

    -t, --timeout=SECONDS       Integer timeout for socket when performing batch
                                operations. [default: 300]

    --cache-dir=DIR             Directory to cache API responses, if provided.



"""
import logging
import os
import sys

import docopt

from . import config, sync

LOG = logging.getLogger(os.path.basename(sys.argv[0]))


def main():
    opts = docopt.docopt(__doc__)

    # Configure logging
    logging.basicConfig(level=logging.WARN if opts["--quiet"] else logging.INFO)

    # HACK: make the googleapiclient.discovery module less spammy in the logs
    logging.getLogger("googleapiclient.discovery").setLevel(logging.WARN)
    logging.getLogger("googleapiclient.discovery_cache").setLevel(logging.ERROR)

    # Convert to integer, any raised exceptions will propagate upwards
    timeout_val = int(opts["--timeout"])

    # Safety check - could write with out of date cached data
    if opts["--write"] and opts["--cache-dir"]:
        LOG.error(
            "Option --write with --cache-dir is blocked to avoid writing outdated cached data."
        )
        exit(1)

    LOG.info("Loading configuration")
    configuration = config.load_configuration(opts["--configuration"])

    # Perform sync
    sync.sync(
        configuration,
        write_mode=opts["--write"],
        timeout=timeout_val,
        group_settings=opts["--group-settings"],
        just_users=opts["--just-users"],
        licensing=opts["--licensing"],
        delete_users=opts["--delete-users"],
        cache_dir=opts["--cache-dir"],
    )
