"""
Utilities for constructing human-friendly names.

"""
import collections
import re


# The human-friendly names constructed by get_names().
Names = collections.namedtuple("Names", "given_name family_name")


def get_names(*, uid, display_name=None, cn=None, sn=None, given_name=None):
    """
    If we only have a uid, this is used for both given name and family name.

    >>> get_names(uid='spqr1')
    Names(given_name='spqr1', family_name='spqr1')
    >>> get_names(uid='spqr1', display_name='spqr1')
    Names(given_name='spqr1', family_name='spqr1')
    >>> get_names(uid='spqr1', display_name='spqr1', cn='spqr1', sn='spqr1')
    Names(given_name='spqr1', family_name='spqr1')
    >>> get_names(uid='spqr1', display_name='')
    Names(given_name='spqr1', family_name='spqr1')
    >>> get_names(uid='spqr1', sn='')
    Names(given_name='spqr1', family_name='spqr1')
    >>> get_names(uid='spqr1', cn='')
    Names(given_name='spqr1', family_name='spqr1')

    "Odd" ASCII characters unsupported by Google are stripped out of names.

    >>> get_names(uid='spqr1', display_name='Stephen @**Quill-Roman**@')
    Names(given_name='Stephen', family_name='Quill-Roman')

    Long names are truncated.

    >>> get_names(uid='spqr1', display_name='Stephen Quill-Roman' + 'X' * 200)
    ... #doctest: +NORMALIZE_WHITESPACE
    Names(given_name='Stephen',
          family_name='Quill-RomanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX')

    If display name has been changed from cn (or we are missing cn) but isn't the default
    we'd get from using preferred first name initial then use that in preference to a given
    name and surname pairing, i.e. assume that's what is really wanted

    >>> get_names(uid='spqr1', cn='Stephen Quill Roman', display_name='Steve Quill-Roman',
    ... sn='Quill Roman', given_name='Stephen')
    Names(given_name='Steve', family_name='Quill-Roman')
    >>> get_names(uid='spqr1', display_name='Steve Quill-Roman', sn='Quill Roman',
    ... given_name='Stephen')
    Names(given_name='Steve', family_name='Quill-Roman')
    >>> get_names(uid='spqr1', cn='S.P. Quill-Roman', display_name='S. Quill-Roman',
    ... sn='Quill-Roman', given_name='Steve')
    Names(given_name='Steve', family_name='Quill-Roman')

    If we have given name and surname then pass these through (after stripping unsupported
    characters and truncating as above)

    >>> get_names(uid='spqr1', sn='Quill-Roman', given_name='Steve')
    Names(given_name='Steve', family_name='Quill-Roman')
    >>> get_names(uid='spqr1', display_name='Stephen Quill Roman', cn='Stephen Quill Roman',
    ... sn='Quill-Roman', given_name='Steve')
    Names(given_name='Steve', family_name='Quill-Roman')

    If we have a display name and surname then try to split it using surname, even if surname
    uses hyphens when display_name uses spaces (assume spaces wanted)

    >>> get_names(uid='spqr1', display_name='Stephen Quill Roman', sn='Quill Roman')
    Names(given_name='Stephen', family_name='Quill Roman')
    >>> get_names(uid='spqr1', display_name='Stephen Quill Roman', sn='Quill-Roman')
    Names(given_name='Stephen', family_name='Quill Roman')

    If we have a display name and given name then try to split it using given name

    >>> get_names(uid='spqr1', display_name='Stephen Quill Roman', given_name='Stephen')
    Names(given_name='Stephen', family_name='Quill Roman')

    However, initials are appended to given_name.

    >>> get_names(uid='spqr1', display_name='Stephen P. Quill Roman', given_name='Stephen')
    Names(given_name='Stephen P.', family_name='Quill Roman')
    >>> get_names(uid='spqr1', display_name='Stephen P.X. Quill Roman', given_name='Stephen')
    Names(given_name='Stephen P.X.', family_name='Quill Roman')

    If this didn't work but we have display name, split it at the final space.

    >>> get_names(uid='spqr1', display_name='Stephen Quill Roman', sn='No match')
    Names(given_name='Stephen Quill', family_name='Roman')
    >>> get_names(uid='spqr1', display_name='Stephen Quill Roman', given_name='No Match')
    Names(given_name='Stephen Quill', family_name='Roman')
    >>> get_names(uid='spqr1', display_name='Stephen Quill Roman')
    Names(given_name='Stephen Quill', family_name='Roman')

    If we don't have a display name but do have a cn then try to split it as we tried with
    display name above, with possible surname and given name helping

    >>> get_names(uid='spqr1', cn='Stephen Quill Roman', sn='Quill Roman')
    Names(given_name='Stephen', family_name='Quill Roman')
    >>> get_names(uid='spqr1', cn='Stephen Quill Roman', sn='Quill-Roman')
    Names(given_name='Stephen', family_name='Quill Roman')
    >>> get_names(uid='spqr1', cn='Stephen Quill Roman', given_name='Stephen')
    Names(given_name='Stephen', family_name='Quill Roman')
    >>> get_names(uid='spqr1', cn='Stephen P. Quill Roman', given_name='Stephen')
    Names(given_name='Stephen P.', family_name='Quill Roman')
    >>> get_names(uid='spqr1', cn='Stephen P.X. Quill Roman', given_name='Stephen')
    Names(given_name='Stephen P.X.', family_name='Quill Roman')
    >>> get_names(uid='spqr1', cn='Stephen Quill Roman', sn='No match')
    Names(given_name='Stephen Quill', family_name='Roman')
    >>> get_names(uid='spqr1', cn='Stephen Quill Roman', given_name='No Match')
    Names(given_name='Stephen Quill', family_name='Roman')
    >>> get_names(uid='spqr1', cn='Stephen Quill Roman')
    Names(given_name='Stephen Quill', family_name='Roman')

    Support Wookey.

    >>> get_names(uid='spqr1', display_name='Wookey')
    Names(given_name='Wookey', family_name='spqr1')
    >>> get_names(uid='spqr1', sn='Wookey')
    Names(given_name='spqr1', family_name='Wookey')
    >>> get_names(uid='spqr1', given_name='Wookey')
    Names(given_name='Wookey', family_name='spqr1')
    >>> get_names(uid='spqr1', cn='Wookey')
    Names(given_name='Wookey', family_name='spqr1')

    """
    # If any of display name, common name, surname or given_name is the same as the
    # uid, proceed as if it were unset. Trim any leading/trailing whitespace at the same time.
    cn = cn.strip() if cn is not None and cn != uid else None
    sn = sn.strip() if sn is not None and sn != uid else None
    display_name = (
        display_name.strip() if display_name is not None and display_name != uid else None
    )
    given_name = given_name.strip() if given_name is not None and given_name != uid else None

    # If any of cn, sn, display_name or given_name are blank, proceed as it they're not set.
    cn = cn if cn != "" else None
    sn = sn if sn != "" else None
    display_name = display_name if display_name != "" else None
    given_name = given_name if given_name != "" else None

    # Function to construct return value from family name and given name. Google names can't be
    # longer than 60 characters so truncate them after cleaning.
    def _make_ret(*, family_name, given_name):
        return Names(family_name=_clean(family_name)[:60], given_name=_clean(given_name)[:40])

    # If we have a display name and it doesn't match cn (or we have no cn) then use this as
    # an indication that this is what the user wants displayed
    if (
        display_name is not None
        and (cn is None or cn != display_name)
        and display_name != jd_import_default(given_name, sn)
    ):
        gn, fn = _split_fullname(display_name, sn=sn, given_name=given_name)
        if gn:
            return _make_ret(family_name=fn, given_name=gn)

    # If we have a sn and given name
    if given_name is not None and sn is not None:
        return _make_ret(family_name=sn, given_name=given_name)

    # If we have a display name then try spliting that
    if display_name is not None:
        gn, fn = _split_fullname(display_name, sn=sn, given_name=given_name)
        if gn:
            return _make_ret(family_name=fn, given_name=gn)

    # If we have a cn then try spliting that
    if cn is not None:
        gn, fn = _split_fullname(cn, sn=sn, given_name=given_name)
        if gn:
            return _make_ret(family_name=fn, given_name=gn)

    # Support Wookey.
    if display_name is not None and " " not in display_name:
        return _make_ret(family_name=uid, given_name=display_name)
    if sn is not None and " " not in sn:
        return _make_ret(family_name=sn, given_name=uid)
    if given_name is not None and " " not in given_name:
        return _make_ret(family_name=uid, given_name=given_name)
    if cn is not None and " " not in cn:
        return _make_ret(family_name=uid, given_name=cn)

    # Give up and return uid for both fields
    return _make_ret(family_name=uid, given_name=uid)


def _split_fullname(fullname, sn=None, given_name=None):
    """
    Return given_name, family_name tuple by splitting fullname with assistance
    of sn and given_name. fullname cannot be None. Returns (None, None) if unable
    to find both parts.

    If just fullname provided the split on last space

    >>> _split_fullname('Stephen Quill Roman')
    ('Stephen Quill', 'Roman')

    If surname provided and matches against end of fullname use it to split

    >>> _split_fullname('Stephen Quill Roman', sn='Quill Roman')
    ('Stephen', 'Quill Roman')

    If surname provided and matches against end of fullname but without hyphens then
    still use it to help split

    >>> _split_fullname('Stephen Quill Roman', sn='Quill-Roman')
    ('Stephen', 'Quill Roman')

    If given name provided and matches against start of fullname use it to split
    but make sure initials stay with given name

    >>> _split_fullname('Stephen Quill Roman', given_name='Stephen')
    ('Stephen', 'Quill Roman')
    >>> _split_fullname('Stephen Quill Roman', given_name='Stephen Quill')
    ('Stephen Quill', 'Roman')
    >>> _split_fullname('Stephen P. Quill Roman', given_name='Stephen')
    ('Stephen P.', 'Quill Roman')
    >>> _split_fullname('Stephen P.Q. Roman', given_name='Stephen')
    ('Stephen P.Q.', 'Roman')

    If surname and given name haven't helped then just split on last space

    >>> _split_fullname('Steve P. Quill-Roman', given_name='Stephen', sn='Quill Roman')
    ('Steve P.', 'Quill-Roman')

    If we've been unable to find non-empty given and family names then return (None, None)

    >>> _split_fullname('Steve')
    (None, None)

    """
    # If we have a sn and fullname ends with it, split out the sn.
    if sn is not None and fullname.endswith(sn):
        gn = fullname[: -len(sn)].strip()
        if gn != "":
            return (gn, sn)

    # If we have a sn with hyphens and fullname ends with sn with hyphens replaced by
    # spaces then split out this from fullname
    if sn is not None and "-" in sn:
        spaced_sn = sn.replace("-", " ")
        if fullname.endswith(spaced_sn):
            gn = fullname[: -len(spaced_sn)].strip()
            if gn != "":
                return (gn, spaced_sn)

    # If we have a given_name and fullname starts with it, split out the given_name
    # then check for initials
    if given_name is not None and fullname.startswith(given_name):
        gn = fullname[: len(given_name)].strip()
        fn = fullname[len(given_name) :].strip()
        first_initial = True
        while re.match(r"^[A-Z]\.", fn):
            gn += " " if first_initial else ""
            first_initial = False
            gn += fn[:2]
            fn = fn[2:].strip()
        if fn != "":
            return (gn, fn)

    # split at last space and see if we have two parts
    components = fullname.split()
    if len(components) > 0:
        fn = components[-1]
        gn = " ".join(components[:-1])
        if gn != "" and fn != "":
            return (gn, fn)

    return (None, None)


def jd_import_default(gn, sn):
    """
    Works out the default display name that Lookup's Jackdaw import would give from given and
    surname.

    We need both value to work this out

    >>> jd_import_default(None, None)
    ''
    >>> jd_import_default('foo', None)
    ''
    >>> jd_import_default(None, 'bar')
    ''

    Use only first initial

    >>> jd_import_default('Anne Marie', 'Smith')
    'A. Smith'

    Cope with hyphenated first names

    >>> jd_import_default('Mary-Anne', 'Jones')
    'M-A. Jones'

    """
    if gn is None or sn is None:
        return ""

    initial = "-".join([name[0].upper() for name in gn.split("-")]) + "."

    return f"{initial} {sn}"


def _clean(s):
    """
    Clean any "bad characters" in names. This pattern is based on the one used by the
    legacy Google authenticator which has this comment:

        Google API doesn't like _some_ characters. The 'documentation'
        (http://www.google.com/support/a/bin/answer.py?answer=33386) says "First and last names
        support unicode/UTF-8 characters, and may contain spaces, letters (a-z), numbers (0-9),
        dashes (-), forward slashes (/), and periods (.)", which makes no sence [sic].
        Experimentation suggests it chokes on '<', '>', and '=', but doesn't mind, e.g. cyrilic
        characters. Compromise by filtering out "!"#$%&'()*+,:;<=>?@[\\]^_`{|}~" - i.e. all the
        'odd' ASCII characters other than the ones explicitly supported.

    We change this to allow "'" since plenty of names have this character. (E.g. "O'Reilly",
    "D'Angelo", etc.)

    >>> _clean('ab@c')
    'abc'
    >>> _clean('a "b" c')
    'a b c'

    """
    return "".join(c for c in s if c not in _CLEAN_BAD_CHARS)


# Characters stripped by _clean. Present as a constant to avoid re-creating it.
_CLEAN_BAD_CHARS = '!"#$%&()*+,:;<=>?@[\\]^_`{|}~'
