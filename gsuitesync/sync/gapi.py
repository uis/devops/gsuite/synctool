"""
Load current user, group and institution data from Google.

"""

import functools
import logging
import re
import socket

from google.oauth2 import service_account
from googleapiclient import discovery

from .. import gapiutil
from .base import ConfigurationStateConsumer
from .utils import (
    cache_to_disk,
    custom_action,
    email_to_gid,
    email_to_uid,
    groupID_regex,
    instID_regex,
)

LOG = logging.getLogger(__name__)

# Scopes required to perform read-only actions.
READ_ONLY_SCOPES = [
    "https://www.googleapis.com/auth/admin.directory.user.readonly",
    "https://www.googleapis.com/auth/admin.directory.group.readonly",
    "https://www.googleapis.com/auth/admin.directory.group.member.readonly",
    "https://www.googleapis.com/auth/apps.groups.settings",
    "https://www.googleapis.com/auth/apps.licensing",
]

# Scopes *in addition to READ_ONLY_SCOPES* required to perform a full update.
WRITE_SCOPES = [
    "https://www.googleapis.com/auth/admin.directory.user",
    "https://www.googleapis.com/auth/admin.directory.group",
    "https://www.googleapis.com/auth/admin.directory.group.member",
]

MYDRIVE_SHARED_ACTION_SCAN = "scan-folders"
MYDRIVE_SHARED_RESULT_NONE = "permissions-none"
MYDRIVE_SHARED_RESULT_REMOVED = "permissions-removed"


class GAPIRetriever(ConfigurationStateConsumer):
    required_config = ("gapi_auth", "gapi_domain", "sync", "licensing")

    def connect(self, timeout=300):
        # load credentials
        self.creds = self._get_credentials(read_only=not self.write_mode)
        # Build the directory service using Google API discovery.
        socket.setdefaulttimeout(timeout)
        directory_service = discovery.build("admin", "directory_v1", credentials=self.creds)

        # Secondary domain for Google groups that come from Lookup groups
        groups_domain = (
            self.gapi_domain_config.groups_domain
            if self.gapi_domain_config.groups_domain is not None
            else self.gapi_domain_config.name
        )
        # Secondary domain for Google groups that come from Lookup institutions
        insts_domain = (
            self.gapi_domain_config.insts_domain
            if self.gapi_domain_config.insts_domain is not None
            else self.gapi_domain_config.name
        )

        # We also build the groupssettings service, which is a parallel API to manage group
        # settings. We do this even if not given `--group-settings` argument as newly created
        # groups will need their settings updated.
        groupssettings_service = discovery.build("groupssettings", "v1", credentials=self.creds)

        # Also build the directory service for using the licensing API
        licensing_service = discovery.build("licensing", "v1", credentials=self.creds)

        # Return components needed for connection with Google API
        self.state.update(
            {
                "directory_service": directory_service,
                "groupssettings_service": groupssettings_service,
                "licensing_service": licensing_service,
                "groups_domain": groups_domain,
                "insts_domain": insts_domain,
            }
        )

    def _get_credentials(self, read_only=True):
        """
        Create a Google credentials object from the configuration. Use *read_only* to indicate if
        read-only credentials are preferred.

        """
        # Load appropriate Google credentials.
        creds_file = self.gapi_auth_config.credentials
        if read_only and self.gapi_auth_config.read_only_credentials is not None:
            creds = self.gapi_auth_config.read_only_credentials
            LOG.info("Using read-only credentials.")

        LOG.info('Loading Google account credentials from "%s"', creds_file)
        creds = service_account.Credentials.from_service_account_file(creds_file)

        # With scopes based on read_only
        creds = creds.with_scopes(READ_ONLY_SCOPES + ([] if read_only else WRITE_SCOPES))

        # Use admin_user if using service account with Domain-Wide Delegation
        if self.gapi_domain_config.admin_user:
            creds = creds.with_subject(self.gapi_domain_config.admin_user)

        return creds

    def retrieve_users(self):
        # Retrieve information on all users excluding domain admins.
        all_google_users = self._filtered_user_list(show_deleted=False)

        # Form mappings from uid to Google user.
        all_google_users_by_uid = {email_to_uid(u["primaryEmail"]): u for u in all_google_users}

        # Form sets of all Google-side uids. The all_google_uids set is all users including
        # the suspended ones and the suspended_google_uids set is only the suspended users. Non
        # suspended users are therefore all_google_uids - suspended_google_uids.
        all_google_uids = set(all_google_users_by_uid.keys())
        suspended_google_uids = {
            uid for uid, u in all_google_users_by_uid.items() if u["suspended"]
        }

        # Form a set of suspended uids that have no mydrive-shared-action or -result
        no_mydrive_shared_settings_uids = {
            uid
            for uid, u in all_google_users_by_uid.items()
            if u["suspended"]
            and not custom_action(u, "mydrive-shared-action")
            and not custom_action(u, "mydrive-shared-result")
        }

        # Form a set of suspended uids that have no mydrive-shared-action and
        # mydrive-shared-result is `permissions-none`
        permission_none_mydrive_shared_result_uids = {
            uid
            for uid, u in all_google_users_by_uid.items()
            if u["suspended"]
            and not custom_action(u, "mydrive-shared-action")
            and custom_action(u, "mydrive-shared-result") == MYDRIVE_SHARED_RESULT_NONE
        }

        # Form a set of suspended uids that have no mydrive-shared-action and
        # mydrive-shared-result is `permissions-removed`
        permission_removed_mydrive_shared_result_uids = {
            uid
            for uid, u in all_google_users_by_uid.items()
            if u["suspended"]
            and not custom_action(u, "mydrive-shared-action")
            and custom_action(u, "mydrive-shared-result") == MYDRIVE_SHARED_RESULT_REMOVED
        }

        # Sanity check. We should not have lost anything. (I.e. the uids should be unique.)
        if len(all_google_uids) != len(all_google_users):
            raise RuntimeError("Sanity check failed: user list changed length")

        # Get recently deleted users so we can restore them instead of recreating a duplicate
        # new account if needed
        all_deleted_google_users = self._filtered_user_list(show_deleted=True)

        # There are potentially multiple deletions of the same user. Assuming we want to restore
        # the last deleted, sort so that the dict references the last one.
        all_deleted_google_users.sort(key=lambda u: u["deletionTime"])
        all_deleted_google_users_by_uid = {
            email_to_uid(u["primaryEmail"]): u for u in all_deleted_google_users
        }
        all_deleted_google_uids = set(all_deleted_google_users_by_uid.keys())

        # Log some stats.
        LOG.info("Total Google users: %s", len(all_google_uids))
        LOG.info("Suspended Google users: %s", len(suspended_google_uids))
        LOG.info(
            "No MyDrive settings suspended Google users: %s", len(no_mydrive_shared_settings_uids)
        )
        LOG.info(
            f"MyDrive shared result {MYDRIVE_SHARED_RESULT_NONE}"
            f" suspended Google users: {len(permission_none_mydrive_shared_result_uids)}"
        )
        LOG.info(
            f"MyDrive shared result {MYDRIVE_SHARED_RESULT_REMOVED}"
            f" suspended Google users: {len(permission_removed_mydrive_shared_result_uids)}"
        )
        LOG.info("Recently Deleted Google users: %s", len(all_deleted_google_uids))

        self.state.update(
            {
                "all_google_users": all_google_users,
                "all_google_users_by_uid": all_google_users_by_uid,
                "all_google_uids": all_google_uids,
                "suspended_google_uids": suspended_google_uids,
                "no_mydrive_shared_settings_uids": no_mydrive_shared_settings_uids,
                "permission_none_mydrive_shared_result_uids": permission_none_mydrive_shared_result_uids,  # NOQA: E501
                "permission_removed_mydrive_shared_result_uids": permission_removed_mydrive_shared_result_uids,  # NOQA: E501
                "all_deleted_google_users_by_uid": all_deleted_google_users_by_uid,
            }
        )

    @cache_to_disk("google_users_{show_deleted}")
    def _filtered_user_list(self, show_deleted=False):
        LOG.info(
            f"Getting information on {'deleted' if show_deleted else 'active'}"
            " Google domain users"
        )
        fields = [
            "id",
            "isAdmin",
            "orgUnitPath",
            "primaryEmail",
            "suspended",
            "lastLoginTime",
            "name(givenName, familyName)",
            "customSchemas",
        ] + (["deletionTime"] if show_deleted else [])
        all_google_users = gapiutil.list_all(
            self.state.directory_service.users().list,
            items_key="users",
            domain=self.gapi_domain_config.name,
            showDeleted=show_deleted,
            query="isAdmin=false",
            fields="nextPageToken,users(" + ",".join(fields) + ")",
            projection="custom",
            customFieldMask="UCam",
            retries=self.sync_config.http_retries,
            retry_delay=self.sync_config.http_retry_delay,
            allow_empty=show_deleted,
        )
        # Strip any "to be ignored" users out of the results.
        # Deleted users don't have an orgUnitPath.
        if not show_deleted and self.sync_config.ignore_google_org_unit_path_regex is not None:
            LOG.info(
                "Ignoring users whose organization unit path matches %r",
                self.sync_config.ignore_google_org_unit_path_regex,
            )
            # Check that all users have an orgUnitPath
            missing_org = [u for u in all_google_users if "orgUnitPath" not in u]
            if len(missing_org) != 0:
                LOG.error(
                    "User entries missing orgUnitPath: %s (starting with %s)",
                    len(missing_org),
                    (
                        missing_org[0]["primaryEmail"]
                        if "primaryEmail" in missing_org[0]
                        else "user with blank email"
                    ),
                )
                raise RuntimeError("Sanity check failed: at least one user is missing orgUnitPath")
            # Remove users matching regex
            regex = re.compile(self.sync_config.ignore_google_org_unit_path_regex)
            all_google_users = [u for u in all_google_users if not regex.match(u["orgUnitPath"])]

        # Strip out any users with uids (extracted from the local-part of the email address) that
        # aren't valid CRSids. These users can't have come from Lookup, and so should not be
        # managed (suspended) by this script.
        all_google_users = [u for u in all_google_users if email_to_uid(u["primaryEmail"])]

        # Sanity check. There should be no admins in the returned results.
        if any(u.get("isAdmin", False) for u in all_google_users):
            raise RuntimeError("Sanity check failed: admin users in user list")

        return all_google_users

    def retrieve_licenses(self):
        if self.licensing_config.product_id is None:
            LOG.info("Skipping licensing assignment as no Product Id set")
            return
        # Retrieve all license assignments
        LOG.info("Getting information on licence assignment for Google domain users")
        LOG.info("Total licence assignments: %s", len(self.all_licence_assignments))

        # Build a map of uids to license SKU. We are only interested in those in our domain
        # and SKUs in our configuration
        skus_by_uid = {
            parts[1]: parts[0]
            for parts in [
                [lic["skuId"]] + lic["userId"].split("@", 1)
                for lic in self.all_licence_assignments
            ]
            if (
                len(parts) == 3
                and parts[2] == self.gapi_domain_config.name
                and parts[0] in self.licensing_config.licensed_skus.keys()
            )
        }

        # Build the set of licensed uids
        licensed_uids = set(skus_by_uid.keys())
        LOG.info("Total licensed uids: %s", len(licensed_uids))

        # Count available licences left for each SKU
        available_by_sku = {
            avail_sku: (total - len({uid for uid, sku in skus_by_uid.items() if sku == avail_sku}))
            for avail_sku, total in self.licensing_config.licensed_skus.items()
        }
        for sku, available in available_by_sku.items():
            LOG.info(f"Licences available in SKU '{sku}': {available}")

        total_available = sum(available_by_sku.values())
        LOG.info(f"Licences available in total: {total_available}")

        self.state.update(
            {
                "google_licensed_uids": licensed_uids,
                "google_skus_by_uid": skus_by_uid,
                "google_available_by_sku": available_by_sku,
            }
        )

    @functools.cached_property
    @cache_to_disk("google_licence_assignments")
    def all_licence_assignments(self):
        fields = ["userId", "skuId"]
        return gapiutil.list_all(
            self.state.licensing_service.licenseAssignments().listForProduct,
            customerId=self.licensing_config.customer_id,
            productId=self.licensing_config.product_id,
            fields="nextPageToken,items(" + ",".join(fields) + ")",
            retries=self.sync_config.http_retries,
            retry_delay=self.sync_config.http_retry_delay,
            page_size=200,
        )

    def retrieve_groups(self):
        # Retrieve information on all Google groups that come from Lookup groups
        LOG.info("Getting information on Google domain groups")
        all_google_groups = [
            g
            for g in self._fetch_groups(domain=self.state.groups_domain)
            if groupID_regex.match(g["email"].split("@")[0])
        ]

        # Append information on all Google groups that come from Lookup institutions
        LOG.info("Getting information on Google domain institutions")
        all_google_groups.extend(
            [
                g
                for g in self._fetch_groups(domain=self.state.insts_domain)
                if instID_regex.match(g["email"].split("@")[0].upper())
            ]
        )

        # Strip out any groups whose email addresses don't match the pattern for groups created
        # from Lookup groupIDs or instIDs, and which therefore should not be managed (deleted) by
        # this script.
        all_google_groups = [g for g in all_google_groups if email_to_gid(g["email"])]

        # Form mappings from gid to Google group.
        all_google_groups_by_gid = {email_to_gid(g["email"]): g for g in all_google_groups}

        # Form sets of all Google-side gids. The all_google_gids set includes both groupIDs and
        # instIDs. Groups in Google do not have any concept of being suspended.
        all_google_gids = set(all_google_groups_by_gid.keys())

        # Sanity check. We should not have lost anything. (I.e. the gids should be unique.)
        if len(all_google_gids) != len(all_google_groups):
            raise RuntimeError("Sanity check failed: group list changed length")

        # Retrieve all Google group memberships. This is a mapping from internal Google group ids
        # to lists of member resources, corresponding to both Lookup groups and institutions.
        all_google_members = self._fetch_group_members([g["id"] for g in all_google_groups])

        # Sanity check. We should have a group members list for each managed group.
        if len(all_google_members) != len(all_google_groups):
            raise RuntimeError(
                "Sanity check failed: groups in members map do not match group list"
            )

        # Log some stats.
        LOG.info("Total Google groups: %s", len(all_google_gids))
        LOG.info(
            "Total Google group members: %s", sum([len(m) for g, m in all_google_members.items()])
        )

        self.state.update(
            {
                "all_google_groups": all_google_groups,
                "all_google_groups_by_gid": all_google_groups_by_gid,
                "all_google_gids": all_google_gids,
                "all_google_members": all_google_members,
            }
        )

    def retrieve_group_settings(self):
        # Retrieve all Google group settings.
        all_google_group_emails = [g["email"] for g in self.state.all_google_groups]
        all_google_group_settings = self._fetch_group_settings(all_google_group_emails)

        # Form a mapping from gid to Google group settings.
        all_google_group_settings_by_gid = {
            email_to_gid(g["email"]): g for g in all_google_group_settings
        }

        # Sanity check. We should have settings for each managed group.
        if len(all_google_group_settings_by_gid) != len(self.state.all_google_groups):
            raise RuntimeError(
                "Sanity check failed: group settings list does not match group list"
            )

        self.state.update(
            {
                "all_google_group_settings_by_gid": all_google_group_settings_by_gid,
            }
        )

    @cache_to_disk("google_groups_{domain}")
    def _fetch_groups(self, domain):
        """
        Function to fetch Google group information from the specified domain

        """
        fields = ["id", "email", "name", "description"]
        return gapiutil.list_all(
            self.state.directory_service.groups().list,
            items_key="groups",
            domain=domain,
            fields="nextPageToken,groups(" + ",".join(fields) + ")",
            retries=self.sync_config.http_retries,
            retry_delay=self.sync_config.http_retry_delay,
        )

    @cache_to_disk("google_group_members")
    def _fetch_group_members(self, ids):
        fields = ["id", "email"]
        return gapiutil.list_all_in_list(
            self.state.directory_service,
            self.state.directory_service.members().list,
            item_ids=ids,
            id_key="groupKey",
            batch_size=self.sync_config.batch_size,
            items_key="members",
            fields="nextPageToken,members(" + ",".join(fields) + ")",
            retries=self.sync_config.http_retries,
            retry_delay=self.sync_config.http_retry_delay,
        )

    @cache_to_disk("google_group_settings")
    def _fetch_group_settings(self, ids):
        fields = ["email", *[k for k in self.sync_config.group_settings.keys()]]
        return gapiutil.get_all_in_list(
            self.state.groupssettings_service,
            self.state.groupssettings_service.groups().get,
            item_ids=ids,
            id_key="groupUniqueId",
            batch_size=self.sync_config.batch_size,
            fields=",".join(fields),
            retries=self.sync_config.http_retries,
            retry_delay=self.sync_config.http_retry_delay,
        )
