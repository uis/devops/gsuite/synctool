"""
Synchronise Google Directory with Lookup using API Gateway.

"""

import logging

from .. import config
from .state import SyncState
from .lookup import LookupRetriever
from .gapi import GAPIRetriever
from .compare import Comparator
from .update import GAPIUpdater

LOG = logging.getLogger(__name__)


def sync(
    configuration,
    *,
    write_mode=False,
    timeout=300,
    group_settings=False,
    just_users=False,
    licensing=False,
    cache_dir=None,
    delete_users=False,
):
    """Perform sync given configuration dictionary."""

    LOG.info("Performing synchronisation:")
    LOG.info("- mode = %s", "WRITE" if write_mode else "READ ONLY")
    LOG.info(
        "- update groups = %s",
        "NO" if just_users else ("YES (with group settings)" if group_settings else "YES"),
    )
    LOG.info("- update licensing = %s", "YES" if licensing else "NO")
    LOG.info("- delete users = %s", "YES" if delete_users else "NO")

    # Parse configuration into Configuration dict of appropriate dataclasses
    configuration = config.parse_configuration(configuration)

    # Class to hold all state that can be updated by the process below then
    # used to do updates
    state = SyncState()

    # Get users and optionally groups from Lookup
    lookup = LookupRetriever(configuration, state, cache_dir=cache_dir)
    lookup.retrieve_users()
    if not just_users:
        lookup.retrieve_groups()

    # Get users and optionally groups from Google
    gapi = GAPIRetriever(configuration, state, write_mode=write_mode, cache_dir=cache_dir)
    gapi.connect(timeout)
    gapi.retrieve_users()
    if licensing:
        gapi.retrieve_licenses()
    if not just_users:
        gapi.retrieve_groups()
        # Optionally get group settings too
        if group_settings:
            gapi.retrieve_group_settings()

    # Get Lookup cancelled dates for suspended Google users
    lookup.retrieve_cancelled_user_dates()

    # Compare users and optionally groups between Lookup and Google
    comparator = Comparator(configuration, state)
    comparator.compare_users()
    if licensing:
        comparator.compare_licensing()
    if not just_users:
        comparator.compare_groups()
        # Optionally compare existing group settings too
        if group_settings:
            comparator.compare_groups_settings()
    # Enforce creation/update limits
    comparator.enforce_limits(just_users, licensing, delete_users)

    # Update Google with necessary updates found doing comparison
    updater = GAPIUpdater(configuration, state, write_mode=write_mode)
    updater.update_users(delete_users)
    if licensing:
        updater.update_licensing()
    if not just_users:
        updater.update_groups()
