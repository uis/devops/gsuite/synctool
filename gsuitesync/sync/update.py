"""
Perform the actual updates in Google (unless not in write_mode)

"""
import crypt
import logging
import secrets

from ..gapiutil import process_requests
from .base import ConfigurationStateConsumer
from .gapi import MYDRIVE_SHARED_ACTION_SCAN
from .utils import gid_to_email, uid_to_email

LOG = logging.getLogger(__name__)


class GAPIUpdater(ConfigurationStateConsumer):
    required_config = ("sync", "gapi_domain", "licensing")

    def update_users(self, delete_users=False):
        process_requests(
            self.state.directory_service,
            self.user_api_requests(delete_users),
            self.sync_config,
            write_mode=self.write_mode,
        )

    def update_licensing(self):
        if self.licensing_config.product_id is None:
            LOG.info("Skipping licensing assignment update as no Product Id set")
            return
        process_requests(
            self.state.licensing_service,
            self.licensing_api_requests(),
            self.sync_config,
            write_mode=self.write_mode,
        )

    def update_groups(self):
        process_requests(
            self.state.directory_service,
            self.group_api_requests(),
            self.sync_config,
            write_mode=self.write_mode,
        )
        # Still need to do this even if `not group_settings` as new groups need their settings
        process_requests(
            self.state.groupssettings_service,
            self.group_settings_api_requests(),
            self.sync_config,
            write_mode=self.write_mode,
        )
        process_requests(
            self.state.directory_service,
            self.member_api_requests(),
            self.sync_config,
            write_mode=self.write_mode,
        )

    def user_api_requests(self, delete_users=False):
        """
        A generator which will generate update(), insert(), delete() and undelete() calls to the
        directory service to perform the actions required to update users

        """
        # Update existing users.
        user_updates = {
            uid: self.state.google_user_updates[uid] for uid in self.state.uids_to_update
        }
        for uid, update in user_updates.items():
            google_id = self.state.all_google_users_by_uid[uid]["id"]
            # Only show the previous parts of name that have been changed
            updated_google_user_name = update.get("name", {})
            previous_google_user_name = self.state.all_google_users_by_uid[uid].get("name", {})
            previous = {
                k: previous_google_user_name.get(k, "")
                for k in ["givenName", "familyName"]
                if k in updated_google_user_name
            }
            LOG.info('Update user "%s": "%r" from "%r"', uid, update, previous)
            yield self.state.directory_service.users().update(userKey=google_id, body=update)

        # Suspend old users
        for uid in self.state.uids_to_suspend:
            google_id = self.state.all_google_users_by_uid[uid]["id"]
            LOG.info('Suspending user: "%s"', uid)
            yield self.state.directory_service.users().update(
                userKey=google_id, body={"suspended": True}
            )

        # Delete suspended users
        if delete_users:
            for uid in self.state.uids_to_delete:
                google_id = self.state.all_google_users_by_uid[uid]["id"]
                LOG.info('Deleting user: "%s"', uid)
                yield self.state.directory_service.users().delete(userKey=google_id)

        # Reactivate returning users
        reactivate_body = {
            "suspended": False,
            "customSchemas": {
                "UCam": {
                    "mydrive-shared-action": None,
                    "mydrive-shared-result": None,
                    "mydrive-shared-filecount": None,
                },
            },
        }
        for uid in self.state.uids_to_reactivate:
            google_id = self.state.all_google_users_by_uid[uid]["id"]
            LOG.info('Reactivating user: "%s"', uid)
            yield self.state.directory_service.users().update(
                userKey=google_id, body=reactivate_body
            )

        # Restore deleted users
        for uid in self.state.uids_to_restore:
            google_id = self.state.all_deleted_google_users_by_uid[uid]["id"]
            LOG.info('Restoring user: "%s" (%s)', uid, google_id)
            yield self.state.directory_service.users().undelete(
                userKey=google_id, body={"orgUnitPath": self.sync_config.new_user_org_unit_path}
            )

        # Create new users
        for uid in self.state.uids_to_add:
            # Generate a random password which is thrown away.
            new_user = {
                **{
                    "hashFunction": "crypt",
                    "password": crypt.crypt(secrets.token_urlsafe(), crypt.METHOD_SHA512),
                    "orgUnitPath": self.sync_config.new_user_org_unit_path,
                },
                **self.state.google_user_creations[uid],
            }
            redacted_user = {**new_user, **{"password": "REDACTED"}}
            LOG.info('Adding user "%s": %s', uid, redacted_user)
            yield self.state.directory_service.users().insert(body=new_user)

        # Mark users to have their MyDrive scanned
        scan_body = {
            "customSchemas": {
                "UCam": {
                    "mydrive-shared-action": MYDRIVE_SHARED_ACTION_SCAN,
                },
            },
        }
        for uid in self.state.uids_to_scan_mydrive:
            google_id = self.state.all_google_users_by_uid[uid]["id"]
            LOG.info('Set MyDrive to be scanned for user: "%s"', uid)
            yield self.state.directory_service.users().update(userKey=google_id, body=scan_body)

    def licensing_api_requests(self):
        """
        A generator which will generate insert() and delete() calls to the licensing
        service to perform the actions required to update licensing

        """
        # Tallying availability
        available_by_sku = self.state.google_available_by_sku

        def next_available_sku():
            # Returns first sku with availability or None if all full
            return next((sku for sku, avail in available_by_sku.items() if avail > 0), None)

        # Remove licences
        for uid in self.state.licences_to_remove:
            userId = f"{uid}@{self.gapi_domain_config.name}"
            skuId = self.state.google_skus_by_uid.get(uid)
            LOG.info('Removing licence from user: "%s" (%s)', uid, skuId)
            yield self.state.licensing_service.licenseAssignments().delete(
                userId=userId, skuId=skuId, productId=self.licensing_config.product_id
            )
            available_by_sku[skuId] += 1

        # Add licences
        for index, uid in enumerate(self.state.licences_to_add):
            userId = f"{uid}@{self.gapi_domain_config.name}"
            skuId = next_available_sku()
            if skuId is None:
                LOG.warning(
                    "No more available licences to allocate missing licenses for %d user(s)",
                    len(self.state.licences_to_add) - index,
                )
                return
            LOG.info('Adding licence to user: "%s" (%s)', uid, skuId)
            yield self.state.licensing_service.licenseAssignments().insert(
                skuId=skuId, productId=self.licensing_config.product_id, body={"userId": userId}
            )
            available_by_sku[skuId] -= 1

    def group_api_requests(self):
        """
        A generator which will generate patch(), insert() and delete() calls to the directory
        service to perform the actions required to update groups

        """
        # Update existing groups
        group_updates = {
            gid: self.state.google_group_updates[gid] for gid in self.state.gids_to_update
        }
        for gid, update in group_updates.items():
            google_id = self.state.all_google_groups_by_gid[gid]["id"]
            LOG.info('Update group "%s": "%r"', gid, update)
            yield self.state.directory_service.groups().patch(groupKey=google_id, body=update)

        # Delete cancelled groups
        for gid in self.state.gids_to_delete:
            google_id = self.state.all_google_groups_by_gid[gid]["id"]
            LOG.info('Deleting group: "%s"', gid)
            yield self.state.directory_service.groups().delete(groupKey=google_id)

        # Create new groups
        for gid in self.state.gids_to_add:
            new_group = self.state.google_group_creations[gid]
            LOG.info('Adding group "%s": %s', gid, new_group)
            yield self.state.directory_service.groups().insert(body=new_group)

    def member_api_requests(self):
        """
        A generator which will generate insert() and delete() calls to the directory service to
        perform the actions required to update group members

        """
        # Insert new members
        for gid, uid in self.state.members_to_insert:
            group_key = gid_to_email(gid, self.state.groups_domain, self.state.insts_domain)
            user_key = uid_to_email(uid, self.gapi_domain_config.name)
            LOG.info('Adding user "%s" to group "%s"', user_key, group_key)
            yield self.state.directory_service.members().insert(
                groupKey=group_key, body={"email": user_key}
            )

        # Delete removed members
        for gid, uid in self.state.members_to_delete:
            group_key = gid_to_email(gid, self.state.groups_domain, self.state.insts_domain)
            user_key = uid_to_email(uid, self.gapi_domain_config.name)
            LOG.info('Removing user "%s" from group "%s"', user_key, group_key)
            yield self.state.directory_service.members().delete(
                groupKey=group_key, memberKey=user_key
            )

    def group_settings_api_requests(self):
        """
        A generator which will generate patch() calls to the groupssettings service to set or
        update the required group settings.

        """
        # Apply all settings to new groups.
        for gid in self.state.gids_to_add:
            email = gid_to_email(gid, self.state.groups_domain, self.state.insts_domain)
            settings = self.sync_config.group_settings
            LOG.info('Updating settings for new group "%s": %s', gid, settings)
            yield self.state.groupssettings_service.groups().patch(
                groupUniqueId=email, body=settings
            )

        # Update existing group settings (will be empty of `not group_settings`)
        for gid in self.state.gids_to_update_group_settings:
            email = gid_to_email(gid, self.state.groups_domain, self.state.insts_domain)
            settings = self.state.group_settings_to_update[gid]
            LOG.info('Updating settings for existing group "%s": %s', gid, settings)
            yield self.state.groupssettings_service.groups().patch(
                groupUniqueId=email, body=settings
            )
