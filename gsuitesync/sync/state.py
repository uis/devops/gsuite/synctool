"""
A dataclass to hold the built up state of Lookup and Google data and needed updates

"""
from typing import Optional
from dataclasses import dataclass, field
from googleapiclient import discovery


@dataclass
class SyncState:
    ################
    # Data retrieved from Lookup
    ################

    # user data
    eligible_uids: set = field(default_factory=set)
    managed_user_entries_by_uid: dict = field(default_factory=dict)
    managed_user_uids: set = field(default_factory=set)
    cancelled_a_month_ago_uids: set = field(default_factory=set)
    cancelled_twenty_four_months_ago_uids: set = field(default_factory=set)

    # licensing
    licensed_uids: set = field(default_factory=set)
    # group data
    eligible_gids: set = field(default_factory=set)
    managed_group_entries_by_gid: dict = field(default_factory=dict)

    ################
    # Components needed when communicating with Google API
    ################
    directory_service: Optional[discovery.Resource] = None
    groupssettings_service: Optional[discovery.Resource] = None
    licensing_service: Optional[discovery.Resource] = None
    groups_domain: str = ""
    insts_domain: str = ""

    ################
    # Data retrieved from Google
    ################

    # user data
    all_google_users: list = field(default_factory=list)
    all_google_users_by_uid: dict = field(default_factory=dict)
    all_google_uids: set = field(default_factory=set)
    suspended_google_uids: set = field(default_factory=set)
    no_mydrive_shared_settings_uids: set = field(default_factory=set)
    permission_none_mydrive_shared_result_uids: set = field(default_factory=set)
    permission_removed_mydrive_shared_result_uids: set = field(default_factory=set)
    all_deleted_google_users_by_uid: dict = field(default_factory=dict)
    # licensing
    google_licensed_uids: set = field(default_factory=set)
    google_skus_by_uid: dict = field(default_factory=dict)
    google_available_by_sku: dict = field(default_factory=dict)
    # group data
    all_google_groups: list = field(default_factory=list)
    all_google_groups_by_gid: dict = field(default_factory=dict)
    all_google_gids: set = field(default_factory=set)
    # group membership data
    all_google_members: dict = field(default_factory=dict)
    # group settings data
    all_google_group_settings_by_gid: dict = field(default_factory=dict)

    ################
    # Results of comparison
    ################

    # updates to users
    google_user_updates: dict = field(default_factory=dict)
    google_user_creations: dict = field(default_factory=dict)
    uids_to_update: set = field(default_factory=set)
    uids_to_add: set = field(default_factory=set)
    uids_to_reactivate: set = field(default_factory=set)
    uids_to_suspend: set = field(default_factory=set)
    uids_to_delete: set = field(default_factory=set)
    uids_to_restore: set = field(default_factory=set)
    uids_to_scan_mydrive: set = field(default_factory=set)
    # licensing
    licences_to_remove: set = field(default_factory=set)
    licences_to_add: set = field(default_factory=set)
    # updates to groups
    google_group_updates: dict = field(default_factory=dict)
    google_group_creations: dict = field(default_factory=dict)
    gids_to_update: set = field(default_factory=set)
    gids_to_add: set = field(default_factory=set)
    gids_to_delete: set = field(default_factory=set)
    # updates to group memberships
    members_to_insert: list = field(default_factory=list)
    members_to_delete: list = field(default_factory=list)
    # updates to group settings
    group_settings_to_update: dict = field(default_factory=dict)
    gids_to_update_group_settings: set = field(default_factory=set)

    ################
    # Allow easy updating from dict
    ################
    def update(self, data: dict):
        for key, value in data.items():
            if hasattr(self, key):
                setattr(self, key, value)
            else:
                raise RuntimeError(f"Attempt to add invalid key '{key}' to state")
