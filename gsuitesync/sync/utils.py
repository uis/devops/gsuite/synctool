import logging
import re
import yaml
from datetime import date
from os import path
from functools import wraps, cache
from dateutil.relativedelta import relativedelta

LOG = logging.getLogger(__name__)


# Functions to translate the unique identifiers of users, groups and institutions in Lookup
# (uids, groupIDs and instIDs) to and from the unique identifiers used in Google (email
# addresses).
#
# For users:   {uid}     <-> {uid}@{domain}
# For groups:  {groupID} <-> {groupID}@{groups_domain}
# For insts:   {instID}  <-> {instID.lower()}@{insts_domain}  (local part must be lowercase)
#
# Additionally, valid uids (CRSids) match the regex [a-z][a-z0-9]{3,7}, valid groupIDs match
# the regex [0-9]{6,8} and valid instIDs match the regex [A-Z][A-Z0-9]+.
#
# Since Lookup institutions become groups in Google, we use common code to sync all Google
# groups, regardless of whether they were groups or institutions in Lookup. In all the code
# that follows, we use "gid" to refer to the unique identifier of the group or institution in
# Lookup (i.e., gid may be either a Lookup groupID or instID).

user_email_regex = re.compile("^[a-z][a-z0-9]{3,7}@.*$")
groupID_regex = re.compile("^[0-9]{6,8}$")
instID_regex = re.compile("^[A-Z][A-Z0-9]+$")


def email_to_uid(email):
    return email.split("@")[0] if user_email_regex.match(email) else None


def email_to_gid(email):
    gid = email.split("@")[0]
    return (
        gid
        if groupID_regex.match(gid)
        else gid.upper()
        if instID_regex.match(gid.upper())
        else None
    )


def uid_to_email(uid, domain):
    return f"{uid}@{domain}"


def gid_to_email(gid, groups_domain, insts_domain):
    return (
        f"{gid}@{groups_domain}"
        if groupID_regex.match(gid)
        else f"{gid.lower()}@{insts_domain}"
        if instID_regex.match(gid)
        else None
    )


# Cached so that result for each `months` is consistent for entire sync run
@cache
def date_months_ago(months):
    return date.today() + relativedelta(months=-months)


def isodate_parse(str):
    return date.fromisoformat(str[:10])


def custom_action(user, property):
    return user.get("customSchemas", {}).get("UCam", {}).get(property)


def cache_to_disk(file_name):
    """
    Decorator for instance method that (if self.cache_dir is set), caches the
    result to a file specified in the decorator parameter in self.cache_dir.

    file_name can contain keyword arguments that will get expanded into actual
    cache file name. e.g. 'google_groups_{domain}'

    """

    def decorator(method):
        @wraps(method)
        def _impl(self, *args, **kwargs):
            # Skip if file caching not enabled
            if not self.cache_dir:
                return method(self, *args, **kwargs)
            # Check if cache file exists1
            full_path = path.join(self.cache_dir, file_name.format(**kwargs)) + ".yaml"
            if path.exists(full_path):
                LOG.info(f"Reading from cache file: {full_path}")
                # Read and return file result instead
                with open(full_path) as fp:
                    data = yaml.unsafe_load(fp)
                return data
            # Get the real data and cache it to file
            data = method(self, *args, **kwargs)
            LOG.info(f"Writing to cache file: {full_path}")
            with open(full_path, "w") as fp:
                yaml.dump(data, fp)
            return data

        return _impl

    return decorator
