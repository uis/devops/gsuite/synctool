"""
Load current user, group and institution data from Lookup.

"""

import collections
import functools
import logging

import yaml
from identitylib.lookup_client import ApiClient as LookupApiClient
from identitylib.lookup_client.api.group_api import GroupApi
from identitylib.lookup_client.api.institution_api import InstitutionApi
from identitylib.lookup_client.api.person_api import PersonApi
from identitylib.lookup_client_configuration import LookupClientConfiguration

from .base import ConfigurationStateConsumer
from .utils import cache_to_disk, date_months_ago, isodate_parse

LOG = logging.getLogger(__name__)

# Default number of entries to request with each API request to list entities
DEFAULT_LIST_FETCH_LIMIT = 1000

# The scheme used to identify users
UID_SCHEME = "crsid"

# Extra attributes to fetch when checking managed entities
USER_FETCH = "all_groups,all_insts,firstName"
USER_FETCH_CANCELLED = "cancelledDate"
MANAGED_USER_FETCH = "firstName"

# Properties containing user/group/institution information in search query results
USER_RESULT_PROPERTY = "people"
GROUP_RESULT_PROPERTY = "groups"
INST_RESULT_PROPERTY = "institutions"

# User and group information we need to populate the Google user directory.
UserEntry = collections.namedtuple(
    "UserEntry", "uid cn sn displayName givenName groupIDs groupNames instIDs licensed"
)
GroupEntry = collections.namedtuple("GroupEntry", "groupID groupName description uids")


class LookupRetriever(ConfigurationStateConsumer):
    required_config = (
        "lookup",
        "api_gateway_auth",
    )
    default_api_params = dict(_request_timeout=120)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.lookup_api_client = self._get_lookup_client()
        self.person_api_client = PersonApi(self.lookup_api_client)
        self.group_api_client = GroupApi(self.lookup_api_client)
        self.inst_api_client = InstitutionApi(self.lookup_api_client)

    def retrieve_users(self):
        """
        Retrieve information about Lookup users.

        """
        # Get a set containing the CRSids of all people who are eligible to be in our GSuite
        # instance. If a user is in GSuite and is *not* present in this list then they are
        # suspended.
        eligible_uids = self.get_eligible_uids()
        LOG.info("Total Lookup user entries: %s", len(eligible_uids))

        # Sanity check: there are some eligible users (else Lookup failure?)
        if len(eligible_uids) == 0:
            raise RuntimeError("Sanity check failed: no users in eligible set")

        # Get a list of managed users. These are all the people who match the "managed_user_filter"
        # in the Lookup settings.
        LOG.info("Reading managed user entries from Lookup")
        managed_user_entries = self.get_managed_user_entries()

        # Form a mapping from uid to managed user.
        managed_user_entries_by_uid = {u.uid: u for u in managed_user_entries}

        # Form a set of all *managed user* uids
        managed_user_uids = set(managed_user_entries_by_uid.keys())
        LOG.info("Total managed user entries: %s", len(managed_user_uids))

        # Sanity check: the managed users should be a subset of the eligible ones.
        if len(managed_user_uids - eligible_uids) != 0:
            raise RuntimeError(
                "Sanity check failed: some managed uids were not in the eligible set"
            )

        # Form a set of licensed user uids (anyone with a non-empty misAffiliation)
        licensed_uids = {u.uid for u in managed_user_entries if u.licensed}
        LOG.info("Total licensed user entries: %s", len(licensed_uids))

        self.state.update(
            {
                "eligible_uids": eligible_uids,
                "managed_user_entries_by_uid": managed_user_entries_by_uid,
                "managed_user_uids": managed_user_uids,
                "licensed_uids": licensed_uids,
            }
        )

        self.has_retrieved_users = True

    def retrieve_cancelled_user_dates(self):
        """
        Retrieve information about cancelled dates for suspended google users and form a set of
        those cancelled more than a month ago and 24 months ago.

        """

        a_month_ago = date_months_ago(1)
        cancelled_a_month_ago_uids = {
            uid
            for uid, cancelledDate in self.cancelled_dates_by_uid.items()
            if cancelledDate and isodate_parse(cancelledDate) < a_month_ago
        }
        LOG.info(
            "Total Lookup users cancelled more than a month ago: %s",
            len(cancelled_a_month_ago_uids),
        )
        self.state.update({"cancelled_a_month_ago_uids": cancelled_a_month_ago_uids})

        twenty_four_months_ago = date_months_ago(24)
        cancelled_twenty_four_months_ago_uids = {
            uid
            for uid, cancelledDate in self.cancelled_dates_by_uid.items()
            if cancelledDate and isodate_parse(cancelledDate) < twenty_four_months_ago
        }
        LOG.info(
            "Total Lookup users cancelled more than 24 months ago: %s",
            len(cancelled_twenty_four_months_ago_uids),
        )
        self.state.update(
            {"cancelled_twenty_four_months_ago_uids": cancelled_twenty_four_months_ago_uids}
        )

    def retrieve_groups(self):
        """
        Retrieve information about Lookup groups and institutions.

        Must be run after `retrieve_users()`.

        """
        # Get a set containing all groupIDs. These are all the groups that are eligible to be in
        # our GSuite instance. If a group is in GSuite and is *not* present in this list then it
        # is deleted.
        LOG.info("Reading eligible group entries from Lookup")
        eligible_groupIDs = self.get_eligible_groupIDs()
        LOG.info("Total Lookup group entries: %s", len(eligible_groupIDs))

        # Get a set containing all instIDs. These are all the institutions that are eligible to be
        # in our GSuite instance. If an institution is in GSuite and is *not* present in this list
        # then the corresponding group is deleted.
        LOG.info("Reading eligible institution entries from Lookup")
        eligible_instIDs = self.get_eligible_instIDs()
        LOG.info("Total Lookup institution entries: %s", len(eligible_instIDs))

        # Add these sets together to form the set of all gids (the IDs of all eligible groups and
        # institutions).
        eligible_gids = eligible_groupIDs | eligible_instIDs
        LOG.info("Total combined Lookup group and institution entries: %s", len(eligible_gids))

        # Get a list of managed groups. These are all the groups that match the
        # "managed_group_filter" in the Lookup settings.
        LOG.info("Reading managed group entries from Lookup")
        managed_group_entries = self.get_managed_group_entries()

        # Form a mapping from groupID to managed group.
        managed_group_entries_by_groupID = {g.groupID: g for g in managed_group_entries}

        # Form a set of all *managed group* groupIDs
        managed_group_groupIDs = set(managed_group_entries_by_groupID.keys())
        LOG.info("Total managed group entries: %s", len(managed_group_groupIDs))
        LOG.info(
            "Total managed group members: %s", sum([len(g.uids) for g in managed_group_entries])
        )

        # Get a list of managed institutions. These are all the institutions that match the
        # "managed_inst_filter" in the Lookup settings.
        LOG.info("Reading managed institution entries from Lookup")
        managed_inst_entries = self.get_managed_inst_entries()

        # Form a mapping from instID to managed institution.
        managed_inst_entries_by_instID = {i.groupID: i for i in managed_inst_entries}

        # Form a set of all *managed institution* instIDs
        managed_inst_instIDs = set(managed_inst_entries_by_instID.keys())
        LOG.info("Total managed institution entries: %s", len(managed_inst_instIDs))
        LOG.info(
            "Total managed institution members: %s",
            sum([len(i.uids) for i in managed_inst_entries]),
        )

        # Add the collections of managed institutions to the collections of managed groups.
        managed_group_entries += managed_inst_entries
        managed_group_entries_by_gid = {
            **managed_group_entries_by_groupID,
            **managed_inst_entries_by_instID,
        }
        managed_group_gids = managed_group_groupIDs | eligible_instIDs
        LOG.info(
            "Total combined managed group and institution entries: %s", len(managed_group_gids)
        )
        LOG.info(
            "Total combined managed group and institution members: %s",
            sum([len(g.uids) for g in managed_group_entries]),
        )

        # Sanity check: the managed groups should be a subset of the eligible ones.
        if len(managed_group_gids - eligible_gids) != 0:
            raise RuntimeError(
                "Sanity check failed: some managed gids were not in the eligible set"
            )

        self.state.update(
            {
                "eligible_gids": eligible_gids,
                "managed_group_entries_by_gid": managed_group_entries_by_gid,
            }
        )

    ###
    # Functions to perform Lookup API calls
    ###
    @functools.cached_property
    @cache_to_disk("lookup_eligible_users")
    def eligible_users_by_uid(self):
        """
        Dictionary mapping CRSid to UserEntry instances. An entry exists in the dictionary for each
        person who is eligible to have a Google account.

        """
        LOG.info("Reading eligible user entries from Lookup")
        return {
            person.identifier.value: UserEntry(
                uid=person.identifier.value,
                cn=person.get("registered_name", ""),
                sn=person.get("surname", ""),
                displayName=person.get("display_name", ""),
                givenName=_extract_attribute(person, "firstName"),
                groupIDs={group.groupid for group in person.groups},
                groupNames={group.name for group in person.groups},
                instIDs={institution.instid for institution in person.institutions},
                licensed=len(person.get("mis_affiliation", "")) > 0,
            )
            for person in self._fetch_all_list_results(
                self.person_api_client.person_search,
                USER_RESULT_PROPERTY,
                self.lookup_config.eligible_user_filter,
                extra_props=dict(fetch=USER_FETCH),
            )
            if person.identifier.scheme == UID_SCHEME and len(person.identifier.value) > 0
        }

    @functools.cached_property
    @cache_to_disk("lookup_cancelled_dates")
    def cancelled_dates_by_uid(self):
        """
        Return a dictionary mapping CRSid to cancelledDate for cancelled users. Limit to
        suspended Google uids.

        """

        LOG.info("Reading cancelled date for suspended Google users from Lookup")

        uids_to_retrieve = list(self.state.suspended_google_uids)
        chunk = 100
        crsid_chunks = [
            uids_to_retrieve[x : x + chunk] for x in range(0, len(uids_to_retrieve), chunk)
        ]

        cancelled_users_to_cancelled_date = {}
        for crsid_chunk in crsid_chunks:
            cancelled_users_to_cancelled_date.update(
                {
                    person.identifier.value: _extract_attribute(person, "cancelledDate")
                    for person in self.person_api_client.person_list_people(
                        ",".join(crsid_chunk),
                        fetch=USER_FETCH_CANCELLED,
                        **self.default_api_params,
                    )
                    .get("result", {})
                    .get("people", [])
                    if person.identifier.scheme == UID_SCHEME
                    and len(person.identifier.value) > 0
                    and person.cancelled
                }
            )

        return cancelled_users_to_cancelled_date

    @functools.cached_property
    @cache_to_disk("lookup_eligible_groups")
    def eligible_groups_by_groupID(self):
        """
        Dictionary mapping groupID to GroupEntry instances. An entry exists in the dictionary for
        each group that is eligible for Google. Information about eligible users is used to
        populate the member list of each group as fetching the member list directly from Lookup
        API results in errors for groups with very large numbers of members (exceeds the API
        Gateway limit for the response size).

        """
        groups = {
            group.groupid: GroupEntry(
                groupID=group.groupid,
                groupName=group.get("name", ""),
                description=group.get("description", ""),
                uids=set(),
            )
            for group in self._fetch_all_list_results(
                self.group_api_client.group_search,
                GROUP_RESULT_PROPERTY,
                self.lookup_config.eligible_group_filter,
            )
            if len(group.groupid) > 0
        }
        for crsid, person in self.eligible_users_by_uid.items():
            for groupID in person.groupIDs:
                if groupID in groups:
                    groups[groupID].uids.add(crsid)
        return groups

    @functools.cached_property
    @cache_to_disk("lookup_eligible_insts")
    def eligible_insts_by_instID(self):
        """
        Dictionary mapping instID to GroupEntry instances. An entry exists in the dictionary for
        each institution that is eligible for Google. Information about eligible users is used to
        populate the member list of each institution as fetching the member list directly from
        Lookup API results in errors for institutions with very large numbers of members (exceeds
        the API Gateway limit for the response size).

        Note that we return GroupEntry instances here since Lookup institutions become groups in
        Google, and this simplifies the sync code by allowing us to handle institutions in the same
        way as groups. The GroupEntry's groupID and groupName fields will be the institution's
        instID and ou (name) respectively. Since Lookup institutions don't have descriptions, we
        set the description field to the institution's name as well (in Google, the description
        allows longer strings, and so will not truncate the name).

        """
        insts = {
            inst.instid: GroupEntry(
                groupID=inst.instid,
                groupName=inst.get("name", ""),
                description=inst.get("name", ""),
                uids=set(),
            )
            for inst in self._fetch_all_list_results(
                self.inst_api_client.institution_search,
                INST_RESULT_PROPERTY,
                self.lookup_config.eligible_inst_filter,
            )
            if len(inst.instid) > 0
        }
        for crsid, person in self.eligible_users_by_uid.items():
            for instID in person.instIDs:
                if instID in insts:
                    insts[instID].uids.add(crsid)
        return insts

    def get_eligible_uids(self):
        """
        Return a set containing all CRSids who are eligible to have a Google account.

        """
        return self.eligible_users_by_uid.keys()

    def get_eligible_groupIDs(self):
        """
        Return a set containing all groupIDs that are eligible for Google.

        """

        return self.eligible_groups_by_groupID.keys()

    def get_eligible_instIDs(self):
        """
        Return a set containing all instIDs that are eligible for Google.

        """
        return self.eligible_insts_by_instID.keys()

    def get_managed_user_entries(self):
        """
        Return a list containing all managed user entries as UserEntry instances.

        """
        return [
            person
            for _, person in self.eligible_users_by_uid.items()
            if self.lookup_config.managed_user_filter is None
            or eval(self.lookup_config.managed_user_filter, {}, person._asdict())
        ]

    def get_managed_group_entries(self):
        """
        Return a list containing all managed group entries as GroupEntry instances.

        """
        return [
            group
            for _, group in self.eligible_groups_by_groupID.items()
            if self.lookup_config.managed_group_filter is None
            or eval(self.lookup_config.managed_group_filter, {}, group._asdict())
        ]

    def get_managed_inst_entries(self):
        """
        Return a list containing all managed institution entries as GroupEntry instances.

        """
        return [
            inst
            for _, inst in self.eligible_insts_by_instID.items()
            if self.lookup_config.managed_inst_filter is None
            or eval(self.lookup_config.managed_inst_filter, {}, inst._asdict())
        ]

    def _fetch_all_list_results(
        self, api_func, result_prop, query, extra_props={}, limit=DEFAULT_LIST_FETCH_LIMIT
    ):
        """
        Repeatedly make an API call with incrementing offset until no more results are returned,
        at which point return all the retrieved results as a single list.

        """
        offset = 0
        result = []
        while offset <= 0 or len(result) >= limit:
            LOG.info(f"Fetching from Lookup API {result_prop}, offset {offset}")
            response = api_func(
                query=query, **self.default_api_params, **extra_props, limit=limit, offset=offset
            )
            result = response.get("result", {}).get(result_prop, [])
            for entity in result:
                yield entity
            offset += limit

    def _get_lookup_client(self):
        """
        Return a Lookup API client instance that can be used to access the Lookup API.

        """
        creds_file = self.api_gateway_auth_config.credentials
        LOG.info('Loading API Gateway app credentials from "%s"', creds_file)
        with open(creds_file, "r") as stream:
            settings = yaml.safe_load(stream)

        config = LookupClientConfiguration(
            settings["client_id"], settings["client_secret"], base_url=settings["base_url"]
        )
        return LookupApiClient(config, pool_threads=10)


def _extract_attribute(entity, attr):
    return next((x.value for x in entity.attributes if x.scheme == attr), "")
