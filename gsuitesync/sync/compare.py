"""
Compute differences between the Lookup and Google data.

"""

import logging
import itertools

from .. import naming
from .base import ConfigurationStateConsumer
from .utils import uid_to_email, gid_to_email, email_to_uid

from .gapi import MYDRIVE_SHARED_RESULT_NONE, MYDRIVE_SHARED_RESULT_REMOVED

LOG = logging.getLogger(__name__)


class Comparator(ConfigurationStateConsumer):
    required_config = ("gapi_domain", "sync", "limits", "licensing")

    def compare_users(self):
        # For each user which exists in Google or the managed user set which is eligible,
        # determine if they need updating/creating. If so, record a patch/insert for the user.
        LOG.info("Calculating updates to users...")
        google_user_updates = {}
        google_user_creations = {}
        google_user_restores = {}
        for uid, managed_user_entry in self.state.managed_user_entries_by_uid.items():
            # Heuristically determine the given and family names.
            names = naming.get_names(
                uid=uid,
                display_name=managed_user_entry.displayName,
                cn=managed_user_entry.cn,
                sn=managed_user_entry.sn,
                given_name=managed_user_entry.givenName,
            )

            # Form expected user resource fields.
            expected_google_user = {
                "name": {
                    "givenName": names.given_name,
                    "familyName": names.family_name,
                },
            }

            # Find existing Google user (if any).
            existing_google_user = self.state.all_google_users_by_uid.get(uid)
            existing_deleted_user = self.state.all_deleted_google_users_by_uid.get(uid)

            if existing_google_user is None and existing_deleted_user is not None:
                # Rather than creating a new duplicated user account restore the original one.
                #
                # It is not possible to update/reactivate the user in the same pass as they are
                # deleted as there is a delay in them becoming available. Undeleted users will
                # therefore remain suspended until the next pass.
                google_user_restores[uid] = existing_deleted_user["id"]
            elif existing_google_user is not None:
                # See if we need to change the existing user
                # Unless anything needs changing, the patch is empty.
                patch = {}

                # Determine how to patch user's name.
                google_name = existing_google_user.get("name", {})
                patch_name = {}
                if google_name.get("givenName") != expected_google_user["name"]["givenName"]:
                    patch_name["givenName"] = names.given_name
                if google_name.get("familyName") != expected_google_user["name"]["familyName"]:
                    patch_name["familyName"] = names.family_name
                if len(patch_name) > 0:
                    patch["name"] = patch_name

                # Only record non-empty patches.
                if len(patch) > 0:
                    google_user_updates[uid] = patch
            else:
                # No existing Google user. Record the new resource. Generate a new user password
                # and send Google the hash. It doesn't matter what this password is since we never
                # have the user log in with it. For password-only applications the user can make
                # use of an application-specific password.
                new_user = {
                    "primaryEmail": uid_to_email(uid, self.gapi_domain_config.name),
                    **expected_google_user,
                }
                google_user_creations[uid] = new_user

        # Form a set of all the uids which need restoring.
        uids_to_restore = set(google_user_restores.keys())
        LOG.info("Number of deleted users to restore: %s", len(uids_to_restore))

        # Form a set of all the uids which need patching.
        uids_to_update = set(google_user_updates.keys())
        LOG.info("Number of existing users to update: %s", len(uids_to_update))

        # Form a set of all the uids which need adding.
        uids_to_add = set(google_user_creations.keys())
        LOG.info("Number of users to add: %s", len(uids_to_add))

        # Form a set of all uids which need reactivating. We reactive users who are in the managed
        # user list *and* the suspended user list.
        uids_to_reactivate = self.state.suspended_google_uids & self.state.managed_user_uids
        LOG.info("Number of users to reactivate: %s", len(uids_to_reactivate))

        # Form a set of all uids which should be suspended. This is all the unsuspended Google uids
        # which do not appear in our eligible user list.
        uids_to_suspend = (
            self.state.all_google_uids - self.state.suspended_google_uids
        ) - self.state.eligible_uids
        LOG.info("Number of users to suspend: %s", len(uids_to_suspend))

        # Users that need their MyDrive scanned for shared files, are those suspended users who
        # have a Lookup cancelledDate over a month ago, haven't been scanned already or already
        # marked to be scanned, and aren't about to be reactivated.
        uids_to_scan_mydrive = (
            self.state.cancelled_a_month_ago_uids & self.state.no_mydrive_shared_settings_uids
        ) - uids_to_reactivate
        LOG.info("Number of users that need MyDrive scanned: %s", len(uids_to_scan_mydrive))

        # Form a set of all uids who have already been suspended, have a mydrive scan result
        # `permissions-none` and have been suspended in lookup for more than 24 months, and aren't
        # about to be reactivated or are marked to be scanned.
        uids_to_delete_with_mydrive_shared_result_permission_none = (
            (self.state.suspended_google_uids - uids_to_reactivate)
            & self.state.permission_none_mydrive_shared_result_uids
            & self.state.cancelled_twenty_four_months_ago_uids
        ) - uids_to_scan_mydrive
        LOG.info(
            f"Number of users to delete (my-shared-result {MYDRIVE_SHARED_RESULT_NONE}):"
            f" {len(uids_to_delete_with_mydrive_shared_result_permission_none)}"
        )

        # Form a set of all uids who have already been suspended, have a mydrive scan result
        # `permissions-removed` and have been suspended in lookup for more than 24 months, and
        # aren't about to be reactivated or are marked to be scanned.
        uids_to_delete_with_mydrive_shared_result_permission_removed = (
            (self.state.suspended_google_uids - uids_to_reactivate)
            & self.state.permission_removed_mydrive_shared_result_uids
            & self.state.cancelled_twenty_four_months_ago_uids
        ) - uids_to_scan_mydrive
        LOG.info(
            f"Number of users to delete (my-shared-result {MYDRIVE_SHARED_RESULT_REMOVED}):"
            f" {len(uids_to_delete_with_mydrive_shared_result_permission_removed)}"
        )

        # Form a super set of uids to delete
        uids_to_delete = (
            uids_to_delete_with_mydrive_shared_result_permission_none
            | uids_to_delete_with_mydrive_shared_result_permission_removed
        )
        LOG.info("Total number of users to delete: %s", len(uids_to_delete))

        self.state.update(
            {
                "google_user_updates": google_user_updates,
                "google_user_creations": google_user_creations,
                "uids_to_update": uids_to_update,
                "uids_to_add": uids_to_add,
                "uids_to_reactivate": uids_to_reactivate,
                "uids_to_suspend": uids_to_suspend,
                "uids_to_delete": uids_to_delete,
                "uids_to_restore": uids_to_restore,
                "uids_to_scan_mydrive": uids_to_scan_mydrive,
            }
        )

    def compare_licensing(self):
        # Compare Lookup and Googles licensed uids
        if self.licensing_config.product_id is None:
            LOG.info("Skipping licensing assignment comparison as no Product Id set")
            return

        # Determining what to do depending on set membership:
        #
        # eligible_uids | managed_uids | licensed_uids | google_licensed_uids | action
        # ----------------------------------------------------------------------------
        #           yes |          yes |           yes |                  yes | nothing
        #           yes |          yes |           yes |                   no | add
        #           yes |          yes |            no |                  yes | remove [1]
        #           yes |          yes |            no |                   no | nothing
        #           yes |           no |            no |                  yes | nothing [2]
        #           yes |           no |            no |                   no | nothing
        #            no |           no |            no |                  yes | remove [3]
        #            no |           no |            no |                   no | nothing
        #
        # [1] - No longer a staff/student in lookup so remove google licence
        # [2] - Not being managed so leave licence alone
        # [3] - Not even eligible for account (now cancelled in lookup, already or will be
        #       suspended/deleted in google) so remove google licence

        # Those licensed in Google but not in Lookup need removing but ignore those eligible
        # but not managed. Also don't try to remove licenses from people that are being deleted
        licences_to_remove = (
            (self.state.google_licensed_uids - self.state.licensed_uids)
            - (self.state.eligible_uids - self.state.managed_user_uids)
            - self.state.uids_to_delete
        )
        LOG.info("Number of licence assignments to remove: %s", len(licences_to_remove))

        # Those licensed in Lookup but not in Google need adding
        licences_to_add = self.state.licensed_uids - self.state.google_licensed_uids
        LOG.info("Number of licence assignments to add: %s", len(licences_to_add))

        # Work out how many licences we will have available after updating
        post_avail = (
            sum(self.state.google_available_by_sku.values())
            + len(licences_to_remove)
            - len(licences_to_add)
        )
        LOG.info("Number of licences available after update: %s", post_avail)
        self.state.update(
            {
                "licences_to_remove": licences_to_remove,
                "licences_to_add": licences_to_add,
            }
        )

    def compare_groups(self):
        # For each group which exists in Google or the managed group set which is eligible,
        # determine if it needs updating/creating. If so, record a patch/insert for the group.
        LOG.info("Calculating updates to groups...")
        google_group_updates = {}
        google_group_creations = {}
        for gid, managed_group_entry in self.state.managed_group_entries_by_gid.items():
            # Form expected group resource fields. The 2 Google APIs we use here to update groups
            # in Google each have different maximum lengths for group names and descriptions, and
            # empirically the APIs don't function properly if either limit is exceeded, so we use
            # the minimum of the 2 documented maximum field lengths (73 characters for names and
            # 300 characters for descriptions).
            #
            # Note that the source of each of these groups may be either a Lookup group or a Lookup
            # institution, which are handled the same here. Technically Lookup institutions do not
            # have descriptions, but the code in lookup.py sets the description from the name for
            # Lookup institutions, which is useful since some institution names do not fit in the
            # Google name field.
            expected_google_group = {
                "name": _trim_text(
                    managed_group_entry.groupName,
                    maxlen=73,
                    suffix=self.sync_config.group_name_suffix,
                ),
                "description": _trim_text(
                    _clean_group_desc(managed_group_entry.description), maxlen=300
                ),
            }

            # Find existing Google group (if any).
            existing_google_group = self.state.all_google_groups_by_gid.get(gid)

            if existing_google_group is not None:
                # See if we need to change the existing group
                # Unless anything needs changing, the patch is empty.
                patch = {}

                if existing_google_group.get("name") != expected_google_group["name"]:
                    patch["name"] = expected_google_group["name"]
                if (
                    existing_google_group.get("description")
                    != expected_google_group["description"]
                ):
                    patch["description"] = expected_google_group["description"]

                # Only record non-empty patches.
                if len(patch) > 0:
                    google_group_updates[gid] = patch
            else:
                # No existing Google group, so create one.
                google_group_creations[gid] = {
                    "email": gid_to_email(gid, self.state.groups_domain, self.state.insts_domain),
                    **expected_google_group,
                }

        # Form a set of all the gids which need patching.
        gids_to_update = set(google_group_updates.keys())
        LOG.info("Number of existing groups to update: %s", len(gids_to_update))

        # Form a set of all the gids which need adding.
        gids_to_add = set(google_group_creations.keys())
        LOG.info("Number of groups to add: %s", len(gids_to_add))

        # Form a set of all gids which need deleting.
        gids_to_delete = self.state.all_google_gids - self.state.eligible_gids
        LOG.info("Number of groups to delete: %s", len(gids_to_delete))

        # For each managed group, determine which members to insert or delete. These are lists of
        # (gid, uid) tuples.
        members_to_insert = []
        members_to_delete = []
        for gid, managed_group_entry in self.state.managed_group_entries_by_gid.items():
            # Find the existing Google group members.
            existing_google_group = self.state.all_google_groups_by_gid.get(gid)
            if existing_google_group:
                existing_members = self.state.all_google_members[existing_google_group["id"]]
                existing_member_uids = set([email_to_uid(m["email"]) for m in existing_members])
            else:
                existing_member_uids = set()

            # Members to insert. This is restricted to the managed user set, so that we don't
            # attempt to insert a member resource for a non-existent user.
            insert_uids = (managed_group_entry.uids - existing_member_uids).intersection(
                self.state.managed_user_uids
            )
            members_to_insert.extend([(gid, uid) for uid in insert_uids])

            # Members to delete. This will delete a member resource when the user is suspended
            # and so we they will get re-added to it if the user is reactivated
            delete_uids = existing_member_uids - managed_group_entry.uids
            members_to_delete.extend([(gid, uid) for uid in delete_uids])

        LOG.info("Number of group members to insert: %s", len(members_to_insert))
        LOG.info("Number of group members to delete: %s", len(members_to_delete))

        self.state.update(
            {
                "google_group_updates": google_group_updates,
                "google_group_creations": google_group_creations,
                "gids_to_update": gids_to_update,
                "gids_to_add": gids_to_add,
                "gids_to_delete": gids_to_delete,
                "members_to_insert": members_to_insert,
                "members_to_delete": members_to_delete,
            }
        )

    def compare_groups_settings(self):
        # Determine changes to existing group settings
        group_settings_to_update = {}
        for gid, settings in self.state.all_google_group_settings_by_gid.items():
            patch = {
                k: v for k, v in self.sync_config.group_settings.items() if settings.get(k) != v
            }
            if len(patch) > 0:
                group_settings_to_update[gid] = patch

        gids_to_update_group_settings = set(group_settings_to_update.keys())
        LOG.info(
            "Number of existing groups to update settings: %s", len(gids_to_update_group_settings)
        )

        self.state.update(
            {
                "group_settings_to_update": group_settings_to_update,
                "gids_to_update_group_settings": gids_to_update_group_settings,
            }
        )

    def enforce_limits(self, just_users, licensing, delete_users):
        # --------------------------------------------------------------------------------------------
        # Enforce limits on how much data to change in Google.
        # --------------------------------------------------------------------------------------------

        # Calculate percentage change to users, groups and group members.
        user_change_percentage = 100.0 * (
            len(
                self.state.uids_to_add
                | self.state.uids_to_update
                | self.state.uids_to_reactivate
                | self.state.uids_to_suspend
                | (self.state.uids_to_delete if delete_users else set())
                | self.state.uids_to_restore
                | self.state.uids_to_scan_mydrive
            )
            / max(1, len(self.state.all_google_uids))
        )
        LOG.info("Configuration will modify %.2f%% of users", user_change_percentage)

        if licensing and self.licensing_config.product_id is not None:
            # Calculate percentage change in licence assignments.
            licence_change_percentage = 100.0 * (
                len(self.state.licences_to_add | self.state.licences_to_remove)
                / max(1, len(self.state.google_licensed_uids))
            )
            LOG.info(
                "Configuration will modify %.2f%% of licence assignments",
                licence_change_percentage,
            )

        if not just_users:
            group_change_percentage = 100.0 * (
                len(self.state.gids_to_add | self.state.gids_to_update | self.state.gids_to_delete)
                / max(1, len(self.state.all_google_gids))
            )
            LOG.info("Configuration will modify %.2f%% of groups", group_change_percentage)

            member_change_percentage = 100.0 * (
                (len(self.state.members_to_insert) + len(self.state.members_to_delete))
                / max(1, sum([len(m) for g, m in self.state.all_google_members.items()]))
            )
            LOG.info("Configuration will modify %.2f%% of group members", member_change_percentage)

        # Enforce percentage change sanity checks.
        if (
            self.limits_config.abort_user_change_percentage is not None
            and user_change_percentage > self.limits_config.abort_user_change_percentage
        ):
            LOG.error(
                "Modification of %.2f%% of users is greater than limit of %.2f%%. Aborting.",
                user_change_percentage,
                self.limits_config.abort_user_change_percentage,
            )
            raise RuntimeError("Aborting due to large user change percentage")

        if licensing and self.licensing_config.product_id is not None:
            if (
                self.limits_config.abort_licence_change_percentage is not None
                and licence_change_percentage > self.limits_config.abort_licence_change_percentage
            ):
                LOG.error(
                    "Modification of %.2f%% of licences is greater than limit of %.2f%%. "
                    "Aborting.",
                    licence_change_percentage,
                    self.limits_config.abort_licence_change_percentage,
                )
                raise RuntimeError("Aborting due to large licence assignment change percentage")

        if not just_users:
            if (
                self.limits_config.abort_group_change_percentage is not None
                and group_change_percentage > self.limits_config.abort_group_change_percentage
            ):
                LOG.error(
                    "Modification of %.2f%% of groups is greater than limit of %.2f%%. Aborting.",
                    group_change_percentage,
                    self.limits_config.abort_group_change_percentage,
                )
                raise RuntimeError("Aborting due to large group change percentage")
            if (
                self.limits_config.abort_member_change_percentage is not None
                and member_change_percentage > self.limits_config.abort_member_change_percentage
            ):
                LOG.error(
                    "Modification of %.2f%% of group members is greater than limit of %.2f%%. "
                    "Aborting.",
                    member_change_percentage,
                    self.limits_config.abort_member_change_percentage,
                )
                raise RuntimeError("Aborting due to large group member change percentage")

        # Cap maximum size of various operations.
        if (
            self.limits_config.max_new_users is not None
            and len(self.state.uids_to_add) > self.limits_config.max_new_users
        ):
            # Ensure that we do not attempt to insert a group member for any of the users not
            # added as a result of this cap, since these users won't exist in Google
            capped_uids_to_add = _limit(self.state.uids_to_add, self.limits_config.max_new_users)
            uids_not_added = self.state.uids_to_add - capped_uids_to_add
            if not just_users:
                self.state.members_to_insert = [
                    (g, u) for g, u in self.state.members_to_insert if u not in uids_not_added
                ]
            self.state.uids_to_add = capped_uids_to_add
            LOG.info("Capped number of new users to %s", len(self.state.uids_to_add))

        if (
            self.limits_config.max_suspended_users is not None
            and len(self.state.uids_to_suspend) > self.limits_config.max_suspended_users
        ):
            self.state.uids_to_suspend = _limit(
                self.state.uids_to_suspend, self.limits_config.max_suspended_users
            )
            LOG.info("Capped number of users to suspend to %s", len(self.state.uids_to_suspend))
        if (
            self.limits_config.max_deleted_users is not None
            and len(self.state.uids_to_delete) > self.limits_config.max_deleted_users
        ):
            self.state.uids_to_delete = _limit(
                self.state.uids_to_delete, self.limits_config.max_deleted_users
            )
            LOG.info("Capped number of users to delete to %s", len(self.state.uids_to_delete))
        if (
            self.limits_config.max_restored_users is not None
            and len(self.state.uids_to_restore) > self.limits_config.max_restored_users
        ):
            self.state.uids_to_restore = _limit(
                self.state.uids_to_restore, self.limits_config.max_restored_users
            )
            LOG.info(
                "Capped number of deleted users to restore to %s", len(self.state.uids_to_restore)
            )
        if (
            self.limits_config.max_reactivated_users is not None
            and len(self.state.uids_to_reactivate) > self.limits_config.max_reactivated_users
        ):
            self.state.uids_to_reactivate = _limit(
                self.state.uids_to_reactivate, self.limits_config.max_reactivated_users
            )
            LOG.info(
                "Capped number of users to reactivate to %s", len(self.state.uids_to_reactivate)
            )
        if (
            self.limits_config.max_mydrive_scan_users is not None
            and len(self.state.uids_to_scan_mydrive) > self.limits_config.max_mydrive_scan_users
        ):
            self.state.uids_to_scan_mydrive = _limit(
                self.state.uids_to_scan_mydrive, self.limits_config.max_mydrive_scan_users
            )
            LOG.info(
                "Capped number of users to mark Mydrive to be scanned to %s",
                len(self.state.uids_to_scan_mydrive),
            )
        if (
            self.limits_config.max_updated_users is not None
            and len(self.state.uids_to_update) > self.limits_config.max_updated_users
        ):
            self.state.uids_to_update = _limit(
                self.state.uids_to_update, self.limits_config.max_updated_users
            )
            LOG.info("Capped number of users to update to %s", len(self.state.uids_to_update))

        # Licences
        if licensing and self.licensing_config.product_id is not None:
            if (
                self.limits_config.max_add_licence is not None
                and len(self.state.licences_to_add) > self.limits_config.max_add_licence
            ):
                self.state.licences_to_add = _limit(
                    self.state.licences_to_add, self.limits_config.max_add_licence
                )
                LOG.info(
                    "Capped number of licences to assign to %s", len(self.state.licences_to_add)
                )
            if (
                self.limits_config.max_remove_licence is not None
                and len(self.state.licences_to_remove) > self.limits_config.max_remove_licence
            ):
                self.state.licences_to_remove = _limit(
                    self.state.licences_to_remove, self.limits_config.max_remove_licence
                )
                LOG.info(
                    "Capped number of licences to remove to %s", len(self.state.licences_to_remove)
                )

        if not just_users:
            if (
                self.limits_config.max_new_groups is not None
                and len(self.state.gids_to_add) > self.limits_config.max_new_groups
            ):
                # Ensure that we do not attempt to insert a group member for any of the groups not
                # added as a result of this cap, since these groups won't exist in Google
                capped_gids_to_add = _limit(
                    self.state.gids_to_add, self.limits_config.max_new_groups
                )
                gids_not_added = self.state.gids_to_add - capped_gids_to_add
                self.state.members_to_insert = [
                    (g, u) for g, u in self.state.members_to_insert if g not in gids_not_added
                ]
                self.state.gids_to_add = capped_gids_to_add
                LOG.info("Capped number of new groups to %s", len(self.state.gids_to_add))

            if (
                self.limits_config.max_deleted_groups is not None
                and len(self.state.gids_to_delete) > self.limits_config.max_deleted_groups
            ):
                self.state.gids_to_delete = _limit(
                    self.state.gids_to_delete, self.limits_config.max_deleted_groups
                )
                LOG.info("Capped number of groups to delete to %s", len(self.state.gids_to_delete))
            if (
                self.limits_config.max_updated_groups is not None
                and len(self.state.gids_to_update) > self.limits_config.max_updated_groups
            ):
                self.state.gids_to_update = _limit(
                    self.state.gids_to_update, self.limits_config.max_updated_groups
                )
                LOG.info("Capped number of groups to update to %s", len(self.state.gids_to_update))
            if (
                self.limits_config.max_inserted_members is not None
                and len(self.state.members_to_insert) > self.limits_config.max_inserted_members
            ):
                self.state.members_to_insert = self.state.members_to_insert[
                    0 : self.limits_config.max_inserted_members
                ]
                LOG.info(
                    "Capped number of group members to insert to %s",
                    len(self.state.members_to_insert),
                )
            if (
                self.limits_config.max_deleted_members is not None
                and len(self.state.members_to_delete) > self.limits_config.max_deleted_members
            ):
                self.state.members_to_delete = self.state.members_to_delete[
                    0 : self.limits_config.max_deleted_members
                ]
                LOG.info(
                    "Capped number of group members to delete to %s",
                    len(self.state.members_to_delete),
                )


def _limit(s, limit):
    """
    Given a set, s, and a numeric limit, return a set which has no more than *limit* elements. The
    exact set of elements retained is not specified.

    >>> s = set('ABCDEFGHIJKLMNOPQ')
    >>> len(s) > 5
    True
    >>> len(_limit(s, 5)) == 5
    True
    >>> len(_limit(s, 500)) == len(s)
    True
    >>> len(_limit(s, 0)) == 0
    True

    All elements of the returned set are taken from input set.

    >>> s_prime = _limit(s, 5)
    >>> s_prime - s
    set()

    """
    return {e for _, e in itertools.takewhile(lambda p: p[0] < limit, enumerate(s))}


def _trim_text(text, *, maxlen, cont="...", suffix=""):
    """
    Trim text to be no more than "maxlen" characters long, terminating it with "cont" if it had
    to be truncated. If supplied, "suffix" is appended to the string after truncating, and the
    truncation point adjusted so that the total length remains less than "maxlen".

    """
    return (
        text[0 : maxlen - len(cont) - len(suffix)] + cont + suffix
        if len(text) + len(suffix) > maxlen
        else text + suffix
    )


def _clean_group_desc(s):
    """
    Clean any "bad characters" in group descriptions.

    Google support (https://support.google.com/a/answer/9193374) says:
    "descriptions can’t contain equal signs (=), or brackets (<,>)"

    >>> _clean_group_desc('a<b>c=d')
    'abcd'

    """
    return "".join(c for c in s if c not in _CLEAN_GROUP_DESC_BAD_CHARS)


# Characters stripped by _clean_group_desc. Present as a constant to avoid re-creating it.
_CLEAN_GROUP_DESC_BAD_CHARS = "=<>"
