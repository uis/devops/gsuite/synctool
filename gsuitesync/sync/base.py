"""
Base classes for retrievers, comparator and updater classes that consume configuration and state.

"""


class ConfigurationStateConsumer:
    required_config = None

    def __init__(self, configuration, state, write_mode=False, cache_dir=None):
        # For convenience, create properties for required configuration
        for c in self.required_config if self.required_config is not None else []:
            setattr(self, f"{c}_config", configuration.get(c, {}))
        self.state = state
        self.write_mode = write_mode
        self.cache_dir = cache_dir
